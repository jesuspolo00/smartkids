<?php
require_once MODELO_PATH . 'conexion.php';

class ModeloHistorial extends conexion
{




//select nombre,fecha_reg from session s,usuarios u where s.id_user = u.id_user
  public static function mostrarDatoshistorialIdModel()
    {
        $tabla  = 'session';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT nombre,fecha_reg FROM " . $tabla . " s ,usuarios u  WHERE s.id_user = u.id_user;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            
            //$preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }


}
