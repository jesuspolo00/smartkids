<?php
require_once MODELO_PATH . 'conexion.php';

class ModeloInventario extends conexion
{

    public static function registrarInventarioModel($datos)
    {
        $tabla = 'inventario';
        $cnx   = conexion::singleton_conexion();
        $sql   = "INSERT INTO " . $tabla . " (descripcion, cantidad, id_user, id_log) VALUES (:d, :c, :idu, :idl)";

        try {
            $preparado = $cnx->preparar($sql);
            $preparado->bindParam(':d', $datos['descripcion']);
            $preparado->bindParam(':c', $datos['cantidad']);
            $preparado->bindParam(':idu', $datos['id_user']);
            $preparado->bindParam(':idl', $datos['id_log']);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarDatosInventarioBuscarModel($consulta)
    {
        $tabla = 'inventario';
        $cnx   = conexion::singleton_conexion();
        $sql   = "SELECT
        iv.*,
        (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user = iv.id_user) AS nom_usuario
        FROM " . $tabla . " iv " . $consulta . " ORDER BY iv.id DESC";

        try {
            $preparado = $cnx->preparar($sql);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarDatosInventarioModel()
    {
        $tabla = 'inventario';
        $cnx   = conexion::singleton_conexion();
        $sql   = "SELECT
        iv.*,
        (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user = iv.id_user) AS nom_usuario
        FROM " . $tabla . " iv  ORDER BY iv.id DESC LIMIT 100";

        try {
            $preparado = $cnx->preparar($sql);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function estadoArticuloModel($datos)
    {
        $tabla = 'inventario';
        $cnx   = conexion::singleton_conexion();
        $sql   = "UPDATE " . $tabla . " SET cantidad = :c, estado = :e, id_log = :idl WHERE id = :id;";

        try {
            $preparado = $cnx->preparar($sql);
            $preparado->bindParam(':c', $datos['cantidad']);
            $preparado->bindParam(':e', $datos['estado']);
            $preparado->bindParam(':idl', $datos['id_log']);
            $preparado->bindParam(':id', $datos['id_articulo']);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function aprobarArticulosModel($id)
    {
        $tabla = 'inventario';
        $cnx   = conexion::singleton_conexion();
        $sql   = "UPDATE " . $tabla . " SET estado = 4 WHERE id = :id;";

        try {
            $preparado = $cnx->preparar($sql);
            $preparado->bindParam(':id', $id);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function eliminarArticulosModel($id)
    {
        $tabla = 'inventario';
        $cnx   = conexion::singleton_conexion();
        $sql   = "DELETE FROM " . $tabla . " WHERE id = :id;";

        try {
            $preparado = $cnx->preparar($sql);
            $preparado->bindParam(':id', $id);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }
}
