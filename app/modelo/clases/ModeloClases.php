<?php
require_once MODELO_PATH . 'conexion.php';

class ModeloClases extends conexion
{

    public static function registrarClaseModel($datos)
    {
        $tabla  = 'clases';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (
        id_profesor,
        id_estudiante,
        horas,
        fecha_inicio,
        fecha_fin,
        dia,
        id_log
        )
        VALUES
        (
        '" . $datos['id_profesor'] . "',
        '" . $datos['id_estudiante'] . "',
        '" . $datos['horas'] . "',
        '" . $datos['fecha_inicio'] . "',
        '" . $datos['fecha_fin'] . "',
        '" . $datos['dia'] . "',
        '" . $datos['id_log'] . "'
        );
        ";

        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarClasesModel($fecha_inicio,$fecha_fin)
    {
        $tabla  = 'clases';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        c.*,
        (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user = c.id_profesor) AS nom_profesor,
        (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user = c.id_estudiante) AS nom_estudiante,
        (SELECT p.nombre FROM foto_perfil p WHERE p.id_user = c.id_profesor AND p.activo = 1 ORDER BY id DESC LIMIT 1) as foto_perfil,
        (SELECT p.nombre FROM foto_perfil p WHERE p.id_user = c.id_estudiante AND p.activo = 1 ORDER BY id DESC LIMIT 1) as foto_estudiante
        FROM clases c where fecha_inicio >='".$fecha_inicio."' and fecha_fin <='".$fecha_fin."' ORDER BY c.id DESC;";

        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarTodasClasesModel()
    {
        $tabla  = 'clases';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        c.*,
        (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user = c.id_profesor) AS nom_profesor,
        (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user = c.id_estudiante) AS nom_estudiante,
        (SELECT p.nombre FROM foto_perfil p WHERE p.id_user = c.id_profesor AND p.activo = 1 ORDER BY id DESC LIMIT 1) as foto_perfil,
        (SELECT p.nombre FROM foto_perfil p WHERE p.id_user = c.id_estudiante AND p.activo = 1 ORDER BY id DESC LIMIT 1) as foto_estudiante
        FROM clases c ORDER BY c.id DESC;";

        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function buscarClasesModel($datos)
    {
        $tabla  = 'clases';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        c.*,
        (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user = c.id_profesor) AS nom_profesor,
        (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user = c.id_estudiante) AS nom_estudiante,
        (SELECT p.nombre FROM foto_perfil p WHERE p.id_user = c.id_profesor AND p.activo = 1 ORDER BY id DESC LIMIT 1) AS foto_perfil
        FROM clases c
        LEFT JOIN usuarios u ON u.id_user = c.id_profesor
        LEFT JOIN usuarios e ON e.id_user = c.id_estudiante
        WHERE CONCAT(u.nombre, ' ', u.apellido, ' ', u.documento, ' ', e.nombre, ' ', e.apellido, ' ', e.documento) LIKE '%" . $datos['buscar'] . "%'
        " . $datos['profesor'] . "
        " . $datos['fecha'] . "
        ORDER BY c.id DESC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarClasesFechaModel($fecha_inicio, $fecha_fin)
    {
        $tabla  = 'clases';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        c.*,
        (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user = c.id_profesor) AS nom_profesor,
        (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user = c.id_estudiante) AS nom_estudiante,
        (SELECT p.nombre FROM foto_perfil p WHERE p.id_user = c.id_profesor AND p.activo = 1 ORDER BY id DESC LIMIT 1) as foto_perfil,
         (SELECT p.nombre FROM foto_perfil p WHERE p.id_user = c.id_estudiante AND p.activo = 1 ORDER BY id DESC LIMIT 1) as foto_estudiante
        FROM clases c WHERE c.fecha_inicio >= '" . $fecha_inicio . "' AND c.fecha_fin <= '" . $fecha_fin . "' ORDER BY c.id DESC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function confirmarClaseModel($datos)
    {
        $tabla  = 'clases';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET estado = :e, observacion = :ob, evidencia = :ev WHERE id = :id;";

        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':e', $datos['estado']);
            $preparado->bindParam(':ob', $datos['observacion']);
            $preparado->bindParam(':ev', $datos['evidencia']);
            $preparado->bindParam(':id', $datos['id_clase']);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function editarClaseModel($datos)
    {
        $tabla  = 'clases';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET id_profesor = :idp, horas = :h, fecha_inicio = :fi, fecha_fin = :ff, dia = :d, id_edit = :ide, fecha_edit = NOW() WHERE id = :id;";

        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':idp', $datos['profesor']);
            $preparado->bindParam(':h', $datos['horas']);
            $preparado->bindParam(':fi', $datos['fecha_inicio']);
            $preparado->bindParam(':ff', $datos['fecha_fin']);
            $preparado->bindParam(':d', $datos['dia']);
            $preparado->bindParam(':ide', $datos['id_log']);
            $preparado->bindParam(':id', $datos['id_clase']);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function aprobarClaseModel($datos)
    {
        $tabla  = 'clases';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET estado = :e, observacion = :ob WHERE id = :id;";

        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':e', $datos['estado']);
            $preparado->bindParam(':ob', $datos['observacion']);
            $preparado->bindParam(':id', $datos['id_clase']);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function comentarClaseModel($datos)
    {
        $tabla  = 'comentarios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (id_clase, id_profesor, comentario, puntuacion, id_user) VALUES (:idc, :idp, :c, :p, :idu)";

        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':idc', $datos['id_clase']);
            $preparado->bindParam(':idp', $datos['id_profesor']);
            $preparado->bindParam(':c', $datos['comentario']);
            $preparado->bindParam(':p', $datos['estrellas']);
            $preparado->bindParam(':idu', $datos['id_user']);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarComentariosClaseModel($id, $id_clase)
    {
        $tabla  = 'comentarios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE id_clase = :idc AND id_user = :id;";

        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->bindParam(':idc', $id_clase);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function comentariosProfesorModel($id)
    {
        $tabla  = 'comentarios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT c.*,
        (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user = c.id_user) AS nom_usuario,
        (SELECT p.nombre FROM foto_perfil p WHERE p.id_user = c.id_user AND p.activo = 1 ORDER BY id DESC LIMIT 1) as foto_perfil
        FROM " . $tabla . " c WHERE c.id_profesor = :id;";

        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarClasesSemanasModel()
    {
        $tabla  = 'clases';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        c.*,
        (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user = c.id_profesor) AS nom_profesor,
        (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user = c.id_estudiante) AS nom_estudiante,
        (SELECT p.nombre FROM foto_perfil p WHERE p.id_user = c.id_profesor AND p.activo = 1 ORDER BY id DESC LIMIT 1) as foto_perfil
        FROM clases c;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }
}
