<?php
date_default_timezone_set('America/Bogota');
require_once MODELO_PATH . 'perfil' . DS . 'ModeloPerfil.php';
require_once CONTROL_PATH . 'hash.php';

class ControlPerfil
{

    private static $instancia;

    public static function singleton_perfil()
    {
        if (!isset(self::$instancia)) {
            $miclase         = __CLASS__;
            self::$instancia = new $miclase;
        }
        return self::$instancia;
    }

    public function mostrarDatosPerfilControl($id)
    {
        $datos = ModeloPerfil::mostrarDatosPerfilModel($id);
        return $datos;
    }

    public function mostrarPerfilesControl()
    {
        $datos = ModeloPerfil::mostrarPerfilesModel();
        return $datos;
    }

    public function guardarPerfilControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log']) &&
            isset($_POST['descripcion']) &&
            !empty($_POST['descripcion'])
        ) {

            $fechareg = date('Y-m-d H:i:s');

            $datos = array(
                'nombre'   => $_POST['descripcion'],
                'fechareg' => $fechareg,
                'user_log' => $_POST['id_log'],
            );

            $guardar = ModeloPerfil::guardarPerfilModelo($datos);
            if ($guardar == true) {
                echo '
                <script>
                ohSnap("Guardado correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("index");
                }
                </script>
                ';
            } else {
                echo '
                <script>
                ohSnap("Ha ocurrido un error", {color: "red"});
                </script>
                ';
            }
        }
    }

    public function editarPerfilesControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_perfil']) &&
            !empty($_POST['id_perfil']) &&
            isset($_POST['nom_edit']) &&
            !empty($_POST['nom_edit'])
        ) {
            $datos = array(
                'id_perfil' => $_POST['id_perfil'],
                'nombre'    => $_POST['nom_edit'],
            );

            $guardar = ModeloPerfil::editarPerfilesModel($datos);
            if ($guardar['guardar'] == true) {
                echo '
                <script>
                ohSnap("Editado correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("index");
                }
                </script>
                ';
            } else {
                echo '
                <script>
                ohSnap("Ha ocurrido un error", {color: "red"});
                </script>
                ';
            }
        }
    }

    public function editarPerfilControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_user']) &&
            !empty($_POST['id_user'])
        ) {

            $pass      = $_POST['password'];
            $conf_pass = $_POST['conf_password'];

            $clavecifrada = Hash::hashpass($pass);
            $clavecifrada = ($pass == $conf_pass && $pass != "" && $conf_pass != "") ? $clavecifrada : $_POST['pass_old'];

            $datos = array(
                'id_user'   => $_POST['id_user'],
                'documento' => $_POST['documento'],
                'nombre'    => $_POST['nombre'],
                'apellido'  => $_POST['apellido'],
                'correo'    => $_POST['correo'],
                'telefono'  => $_POST['telefono'],
                'usuario'   => $_POST['usuario'],
                'pass'      => $clavecifrada,
                'perfil'    => $_POST['perfil'],
            );

            if (!empty($_FILES['archivo']['name'])) {
                $datos_foto = array(
                    'archivo' => $_FILES['archivo']['name'],
                    'id_user' => $_POST['id_user'],
                    'tipo'    => 'archivo',
                );

                $this->guardarFotoPerfilControl($datos_foto);
            }

            $guardar = ModeloPerfil::editarPerfilModel($datos);

            if ($guardar == true) {
                echo '
                <script>
                ohSnap("Modificado correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("index");
                }
                </script>
                ';
            } else {
                echo '
                <script>
                ohSnap("Ha ocurrido un error", {color: "red"});
                </script>
                ';
            }
        }
    }

    public function eliminarPerfilControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_perfil']) &&
            !empty($_POST['id_perfil'])
        ) {

            $id_perfil = filter_input(INPUT_POST, 'id_perfil', FILTER_SANITIZE_NUMBER_INT);
            $result    = ModeloPerfil::eliminarPerfilModelo($id_perfil);

            if ($result == true) {
                $r = "ok";
            } else {
                $r = "No";
            }
            return $r;
        }
    }

    public function guardarFotoPerfilControl($datos)
    {

        $nom_arch = $datos['archivo'];

        $info       = new SplFileInfo($nom_arch);
        $fecha_arch = date('YmdHis');

        $nombre_archivo = strtolower(md5($datos['id_user'] . '_' . $fecha_arch)) . '.' . $info->getExtension();

        $datos_archivo = array(
            'nombre'  => $nombre_archivo,
            'id_user' => $datos['id_user'],
        );

        $guardar_cert = ModeloPerfil::guardarFotoPerfilModel($datos_archivo);

        if ($guardar_cert == true) {

            $carp_destino = PUBLIC_PATH_ARCH . 'upload' . DS;
            $ruta_img     = $carp_destino . $nombre_archivo;

            if (is_uploaded_file($_FILES[$datos['tipo']]['tmp_name'])) {
                move_uploaded_file($_FILES[$datos['tipo']]['tmp_name'], $ruta_img);
            }

            return true;
        }
    }
}
