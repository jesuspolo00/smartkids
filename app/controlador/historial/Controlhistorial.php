<?php
date_default_timezone_set('America/Bogota');
require_once MODELO_PATH . 'historial' . DS . 'Modelohistorial.php';
require_once CONTROL_PATH . 'hash.php';

class Controlhistorial
{

    private static $instancia;

    public static function singleton_historial()
    {
        if (!isset(self::$instancia)) {
            $miclase         = __CLASS__;
            self::$instancia = new $miclase;
        }
        return self::$instancia;
    }

    public function mostrarDatoshistorialControl()
    {
        $datos = Modelohistorial::mostrarDatoshistorialIdModel();
        return $datos;
    }

   
    }

