<?php
date_default_timezone_set('America/Bogota');
require_once MODELO_PATH . 'usuario' . DS . 'ModeloUsuario.php';
require_once CONTROL_PATH . 'hash.php';

class ControlUsuario
{

    private static $instancia;

    public static function singleton_usuario()
    {
        if (!isset(self::$instancia)) {
            $miclase         = __CLASS__;
            self::$instancia = new $miclase;
        }
        return self::$instancia;
    }

    public function mostrarDatosUsuariosControl()
    {
        $datos = ModeloUsuario::mostrarDatosUsuariosModel();
        return $datos;
    }

    public function mostrarDatosUsuariosIdControl($id)
    {
        $datos = ModeloUsuario::mostrarDatosUsuariosIdModel($id);
        return $datos;
    }

    public function contarUsuariosActivosControl()
    {
        $datos = ModeloUsuario::contarUsuariosActivosModelo();
        return $datos;
    }

    public function contarUsuariosInactivosControl()
    {
        $datos = ModeloUsuario::contarUsuariosInactivosModelo();
        return $datos;
    }

    public function contarUsuariosTotalesControl()
    {
        $datos = ModeloUsuario::contarUsuariosTotalesModelo();
        return $datos;
    }

    public function restablecerPassControl($id)
    {
        $pass  = Hash::hashpass('123456');
        $datos = ModeloUsuario::restablecerPassModel($id, $pass);
        return $datos;
    }

    public function registrarUsuarioControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log']) &&
            isset($_POST['documento']) &&
            !empty($_POST['documento'])
        ) {

            $pass         = $_POST['password'];
            $conf_pass    = $_POST['conf_password'];
            $usuario      = str_replace(' ', '', $_POST['usuario']);
            $clavecifrada = Hash::hashpass($conf_pass);
            $id_log       = $_POST['id_log'];

            if ($conf_pass != $pass) {
                echo '
                <script>
                ohSnap("Las contraseñas no coinciden!", {color: "red"});
                </script>
                ';
            } else {

                $user_existe = ModeloUsuario::verififcarUsuarioModelo($usuario);

                if ($user_existe['user'] == $usuario) {
                    echo '
                    <script>
                    ohSnap("Usuario ya existe!", {color: "red"});
                    </script>
                    ';
                } else {
                    $datos = array(
                        'documento' => $_POST['documento'],
                        'nombre'    => $_POST['nombre'],
                        'apellido'  => $_POST['apellido'],
                        'correo'    => $_POST['correo'],
                        'telefono'  => $_POST['telefono'],
                        'user'      => $_POST['usuario'],
                        'pass'      => $clavecifrada,
                        'perfil'    => $_POST['perfil'],
                        'id_log'    => $id_log,
                    );

                    $guardar = ModeloUsuario::registrarUsuarioModel($datos);

                    if ($guardar == true) {
                        echo '
                        <script>
                        ohSnap("Registrado correctamente!", {color: "green", "duration": "1000"});
                        setTimeout(recargarPagina,1050);

                        function recargarPagina(){
                            window.location.replace("index");
                        }
                        </script>
                        ';
                    } else {
                        echo '
                        <script>
                        ohSnap("Ha ocurrido un error!", {color: "red"});
                        </script>
                        ';
                    }
                }
            }
        } else {
        }
    }

    public function editarUsuarioControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_user']) &&
            !empty($_POST['id_user'])
        ) {

            $url = ($_POST['url'] == 0) ? 'index' : 'estudiante';

            $datos = array(
                'id_user'   => $_POST['id_user'],
                'nombre'    => $_POST['nombre_edit'],
                'apellido'  => $_POST['apellido_edit'],
                'telefono'  => $_POST['telefono_edit'],
                'perfil'    => $_POST['perfil_edit'],
                'correo'    => $_POST['correo_edit'],
                'direccion' => $_POST['direccion'],
                'acudiente' => $_POST['acudiente'],
            );

            $editar = ModeloUsuario::editarUsuarioModelo($datos);

            if ($editar == true) {
                echo '
                <script>
                ohSnap("Editado correctamente!", {color: "green"});
                setTimeout(recargarPagina,2000);
                function recargarPagina(){
                    window.location.replace("' . $url . '");
                }
                </script>
                ';
            } else {
                echo '
                <script>
                ohSnap("Ha ocurrido un error!", {color: "red"});
                </script>
                ';
            }

        }
    }

    public function verificarUsuarioControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['usuario']) &&
            !empty($_POST['usuario'])
        ) {

            $user   = filter_input(INPUT_POST, 'usuario', FILTER_SANITIZE_NUMBER_INT);
            $result = ModeloUsuario::verififcarUsuarioModelo($user);
            if ($result['id'] != "") {
                $r = "ok";
            } else {
                $r = "No";
            }
            return $r;
        }
    }

    public function verificarDocumentoControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['documento']) &&
            !empty($_POST['documento'])
        ) {

            $documento = filter_input(INPUT_POST, 'documento', FILTER_SANITIZE_NUMBER_INT);
            $result    = ModeloUsuario::verificarDocumentoModelo($documento);

            if ($result['documento'] == $documento) {
                $r = "ok";
            } else {
                $r = "No";
            }
            return $r;
        }
    }

    public function inactivarUsuarioControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_user']) &&
            !empty($_POST['id_user'])
        ) {
            $fechareg = date('Y-m-d H:i:s');
            $id_user  = filter_input(INPUT_POST, 'id_user', FILTER_SANITIZE_NUMBER_INT);

            $datos = array(
                'fecha_inactivo' => $fechareg,
                'id_user'        => $id_user,
            );

            $result = ModeloUsuario::inactivarUsuarioModelo($datos);

            if ($result == true) {
                $r = "ok";
            } else {
                $r = "No";
            }
            return $r;
        }
    }

    public function activarUsuarioControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_user']) &&
            !empty($_POST['id_user'])
        ) {
            $fechareg = date('Y-m-d H:i:s');
            $id_user  = filter_input(INPUT_POST, 'id_user', FILTER_SANITIZE_NUMBER_INT);

            $datos = array(
                'fecha_activo' => $fechareg,
                'id_user'      => $id_user,
            );

            $result = ModeloUsuario::activarUsuarioModelo($datos);

            if ($result == true) {
                $r = "ok";
            } else {
                $r = "No";
            }
            return $r;
        }
    }

    public function eliminarUsuarioControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_user']) &&
            !empty($_POST['id_user'])
        ) {

            $id_user = filter_input(INPUT_POST, 'id_user', FILTER_SANITIZE_NUMBER_INT);
            $result  = ModeloUsuario::eliminarUsuarioModelo($id_user);

            if ($result == true) {
                $r = "ok";
            } else {
                $r = "No";
            }
            return $r;
        }
    }
}
