<?php
date_default_timezone_set('America/Bogota');
require_once MODELO_PATH . 'clases' . DS . 'ModeloClases.php';
require_once CONTROL_PATH . 'numeros.php';

class ControlClases
{

    private static $instancia;

    public static function singleton_clases()
    {
        if (!isset(self::$instancia)) {
            $miclase         = __CLASS__;
            self::$instancia = new $miclase;
        }
        return self::$instancia;
    }

    public function mostrarClasesControl()
    {
        $fecha_inicio = inicio_semana(date('Y-m-d'));
        $fecha_fin = fin_semana(date('Y-m-d'));
        $mostrar = ModeloClases::mostrarClasesModel($fecha_inicio,$fecha_fin);
        return $mostrar;
    }

    public function mostrarTodasClasesControl()
    {
        $mostrar = ModeloClases::mostrarTodasClasesModel();
        return $mostrar;
    }

    public function mostrarClasesSemanasControl()
    {
        $mostrar = ModeloClases::mostrarClasesSemanasModel();
        return $mostrar;
    }

    public function buscarClasesControl($datos)
    {
        $fecha    = (!empty($datos['fecha'])) ? ' AND c.fecha_inicio <= "' . $datos['fecha'] . '" AND c.fecha_fin >= "' . $datos['fecha'] . '"' : '';
        $profesor = (!empty($datos['profesor'])) ? ' AND c.id_profesor = ' . $datos['profesor'] : '';

        $datos = array('fecha' => $fecha, 'profesor' => $profesor, 'buscar' => $datos['buscar']);

        $mostrar = ModeloClases::buscarClasesModel($datos);
        return $mostrar;
    }

    public function mostrarClasesFechaControl($fecha_inicio, $fecha_fin)
    {
        $mostrar = ModeloClases::mostrarClasesFechaModel($fecha_inicio, $fecha_fin);
        return $mostrar;
    }

    public function mostrarComentariosClaseControl($id, $id_clase)
    {
        $mostrar = ModeloClases::mostrarComentariosClaseModel($id, $id_clase);
        return $mostrar;
    }

    public function comentariosProfesorControl($id)
    {
        $mostrar = ModeloClases::comentariosProfesorModel($id);
        return $mostrar;
    }

    public function registrarClaseControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            foreach ($_POST['dia'] as $dia) {

                $datos = array(
                    'id_profesor'   => $_POST['profesor'],
                    'id_estudiante' => $_POST['estudiante'],
                    'horas'         => $_POST['horas'],
                    'fecha_inicio'  => $_POST['fecha_inicio'],
                    'fecha_fin'     => $_POST['fecha_fin'],
                    'dia'           => $dia,
                    'id_log'        => $_POST['id_log'],
                );

                $guardar = ModeloClases::registrarClaseModel($datos);
            }

            if ($guardar == true) {
                echo '
                <script>
                ohSnap("Registrado correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("index");
                }
                </script>
                ';
            } else {
                echo '
                <script>
                ohSnap("Ha ocurrido un error!", {color: "red"});
                </script>
                ';
            }

        }
    }

    public function confirmarClaseControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_clase']) &&
            !empty($_POST['id_clase'])
        ) {

            $nombre_archivo = '';

            if (isset($_FILES['evidencia']['name']) && !empty($_FILES['evidencia']['name'])) {
                $info       = $_FILES['evidencia']['name'];
                $extension  = pathinfo($info, PATHINFO_EXTENSION);
                $fecha_arch = date('YmdHis');

                $nombre_archivo = strtolower(md5($_POST['id_clase'] . '_' . $fecha_arch)) . '.' . $extension;

                $carp_destino = PUBLIC_PATH_ARCH . 'upload' . DS;
                $ruta_img     = $carp_destino . $nombre_archivo;

                if (is_uploaded_file($_FILES['evidencia']['tmp_name'])) {
                    move_uploaded_file($_FILES['evidencia']['tmp_name'], $ruta_img);
                }

            }

            $datos = array(
                'id_clase'    => $_POST['id_clase'],
                'estado'      => $_POST['estado'],
                'observacion' => $_POST['observacion'],
                'evidencia'   => $nombre_archivo,
            );

            $guardar = ModeloClases::confirmarClaseModel($datos);

            if ($guardar == true) {
                echo '
                <script>
                ohSnap("Registrado correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("inicio");
                }
                </script>
                ';
            } else {
                echo '
                <script>
                ohSnap("Ha ocurrido un error!", {color: "red"});
                </script>
                ';
            }
        }
    }

    public function editarClaseControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_clase']) &&
            !empty($_POST['id_clase'])
        ) {
            $datos = array(
                'id_clase'     => $_POST['id_clase'],
                'id_log'       => $_POST['id_log'],
                'profesor'     => $_POST['profesor_edit'],
                'horas'        => $_POST['horas'],
                'fecha_inicio' => $_POST['fecha_inicio'],
                'fecha_fin'    => $_POST['fecha_fin'],
                'dia'          => $_POST['dia'],
            );

            $guardar = ModeloClases::editarClaseModel($datos);

            if ($guardar == true) {
                echo '
                <script>
                ohSnap("Editado correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("index");
                }
                </script>
                ';
            } else {
                echo '
                <script>
                ohSnap("Ha ocurrido un error!", {color: "red"});
                </script>
                ';
            }
        }
    }

    public function aprobarClaseControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_clase']) &&
            !empty($_POST['id_clase'])
        ) {
            $datos = array(
                'id_clase'    => $_POST['id_clase'],
                'observacion' => $_POST['observacion'],
                'estado'      => 3,
            );

            $guardar = ModeloClases::aprobarClaseModel($datos);

            if ($guardar == true) {
                echo '
                <script>
                ohSnap("Aprobado correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("index");
                }
                </script>
                ';
            } else {
                echo '
                <script>
                ohSnap("Ha ocurrido un error!", {color: "red"});
                </script>
                ';
            }
        }
    }

    public function comentarClaseControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_clase']) &&
            !empty($_POST['id_clase'])
        ) {
            $datos = array(
                'id_user'     => $_POST['id_log'],
                'id_profesor' => $_POST['id_profesor'],
                'id_clase'    => $_POST['id_clase'],
                'comentario'  => $_POST['comentario'],
                'estrellas'   => $_POST['estrellas'],
            );

            $guardar = ModeloClases::comentarClaseModel($datos);

            if ($guardar == true) {
                echo '
                <script>
                ohSnap("Comentado correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("inicio");
                }
                </script>
                ';
            } else {
                echo '
                <script>
                ohSnap("Ha ocurrido un error!", {color: "red"});
                </script>
                ';
            }
        }
    }
}
