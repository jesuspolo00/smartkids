
<?php
date_default_timezone_set('America/Bogota');
require_once MODELO_PATH . 'inventario' . DS . 'ModeloInventario.php';
require_once CONTROL_PATH . 'hash.php';

class ControlInventario
{

    private static $instancia;

    public static function singleton_inventario()
    {
        if (!isset(self::$instancia)) {
            $miclase         = __CLASS__;
            self::$instancia = new $miclase;
        }
        return self::$instancia;
    }

    public function mostrarDatosInventarioBuscarControl($usuario, $articulo)
    {

        if ($usuario != '' && $articulo == '') {
            $consulta = ' WHERE iv.id_user IN(' . $usuario . ')';
        } else if ($articulo != '' && $usuario == '') {
            $consulta = ' WHERE iv.descripcion LIKE "%' . $articulo . '%"';
        } else {
            $consulta = ' WHERE iv.descripcion LIKE "%' . $articulo . '%" AND iv.id_user IN(' . $usuario . ')';
        }

        $mostrar = ModeloInventario::mostrarDatosInventarioBuscarModel($consulta);
        return $mostrar;
    }

    public function mostrarDatosInventarioControl()
    {
        $mostrar = ModeloInventario::mostrarDatosInventarioModel();
        return $mostrar;
    }

    public function registrarInventarioControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {
            $datos = array(
                'id_log'      => $_POST['id_log'],
                'descripcion' => $_POST['descripcion'],
                'cantidad'    => $_POST['cantidad'],
                'id_user'     => $_POST['usuario_id'],
            );

            $guardar = ModeloInventario::registrarInventarioModel($datos);

            if ($guardar == true) {
                echo '
                <script>
                ohSnap("Registrado correctamente!", {color: "green"});
                setTimeout(recargarPagina,2000);
                function recargarPagina(){
                    window.location.replace("index");
                }
                </script>
                ';
            } else {
                echo '
                <script>
                ohSnap("Ha ocurrido un error!", {color: "red"});
                </script>
                ';
            }
        }
    }

    public function estadoArticuloControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_articulo']) &&
            !empty($_POST['id_articulo'])
        ) {

            $url = ($_POST['inicio'] == 0) ? 'index' : 'inicio';

            if ($_POST['estado'] == 3) {

                if ($_POST['cant_dism'] == 0) {
                    echo '
                    <script>
                    ohSnap("Cantidad no valida!", {color: "red"});
                    setTimeout(recargarPagina,2000);
                    function recargarPagina(){
                        window.location.replace("' . $url . '");
                    }
                    </script>
                    ';
                } else {
                    $cantidad = $_POST['cant_dism'] + $_POST['cant_actual'];

                    $datos = array(
                        'id_articulo' => $_POST['id_articulo'],
                        'id_log'      => $_POST['id_log'],
                        'cantidad'    => $cantidad,
                        'estado'      => $_POST['estado'],
                    );

                    $guardar = ModeloInventario::estadoArticuloModel($datos);

                    if ($guardar == true) {
                        echo '
                        <script>
                        ohSnap("Actualizado correctamente!", {color: "green"});
                        setTimeout(recargarPagina,2000);
                        function recargarPagina(){
                            window.location.replace("' . $url . '");
                        }
                        </script>
                        ';
                    } else {
                        echo '
                        <script>
                        ohSnap("Ha ocurrido un error!", {color: "red"});
                        </script>
                        ';
                    }
                }

            } else if ($_POST['estado'] == 0) {

                if ($_POST['cant_dism'] > $_POST['cant_actual'] || $_POST['cant_dism'] == 0) {

                    echo '
                    <script>
                    ohSnap("Cantidad no valida!", {color: "red"});
                    setTimeout(recargarPagina,2000);
                    function recargarPagina(){
                        window.location.replace("' . $url . '");
                    }
                    </script>
                    ';

                } else {

                    $cantidad = $_POST['cant_actual'] - $_POST['cant_dism'];

                    $datos = array(
                        'id_articulo' => $_POST['id_articulo'],
                        'id_log'      => $_POST['id_log'],
                        'cantidad'    => $cantidad,
                        'estado'      => $_POST['estado'],
                    );

                    $guardar = ModeloInventario::estadoArticuloModel($datos);

                    if ($guardar == true) {
                        echo '
                        <script>
                        ohSnap("Actualizado correctamente!", {color: "green"});
                        setTimeout(recargarPagina,2000);
                        function recargarPagina(){
                            window.location.replace("' . $url . '");
                        }
                        </script>
                        ';
                    } else {
                        echo '
                        <script>
                        ohSnap("Ha ocurrido un error!", {color: "red"});
                        </script>
                        ';
                    }
                }
            }

        }
    }

    public function aprobarArticulosControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id']) &&
            !empty($_POST['id'])
        ) {
            $guardar = ModeloInventario::aprobarArticulosModel($_POST['id']);
            return $guardar;
        }
    }

    public function eliminarArticulosControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id']) &&
            !empty($_POST['id'])
        ) {
            $guardar = ModeloInventario::eliminarArticulosModel($_POST['id']);
            return $guardar;
        }
    }
}
