$(document).ready(function() {
    var tipoEvento = ((document.ontouchstart !== null) ? 'click' : 'touchstart');
    /*---------------------------*/
    $(".aprobar").on(tipoEvento, function() {
        let id = $(this).attr('id');
        aprobarArticulos(id);
    });
    /*--------------------------------*/
    function aprobarArticulos(id) {
        try {
            $.ajax({
                url: '../vistas/ajax/inventario/aprobar.php',
                method: 'POST',
                data: {
                    'id': id
                },
                cache: false,
                dataType: 'json',
                success: function(resultado) {
                    if (resultado.mensaje == 'ok') {
                        ohSnap("Aprobado correctamente!", {
                            color: "green"
                        });
                        $(".span" + id).html('<span class="badge badge-success">Aprobado</span>');
                        $("#" + id).hide();
                        //   setTimeout(recargarPagina, 2000);
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }
    /*---------------------------*/
    $(".eliminar").on(tipoEvento, function() {
        let id = $(this).attr('id');
        eliminarArticulo(id);
    });
    /*--------------------------------*/
    function eliminarArticulo(id) {
        try {
            $.ajax({
                url: '../vistas/ajax/inventario/eliminar.php',
                method: 'POST',
                data: {
                    'id': id
                },
                cache: false,
                dataType: 'json',
                success: function(resultado) {
                    if (resultado.mensaje == 'ok') {
                        ohSnap("Eliminado correctamente!", {
                            color: "green"
                        });
                        //$(".span" + id).html('<span class="badge badge-success">Aprobado</span>');
                        $(".articulo" + id).fadeOut();
                        //   setTimeout(recargarPagina, 2000);
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }

    function recargarPagina() {
        window.location.replace("index");
    }
});