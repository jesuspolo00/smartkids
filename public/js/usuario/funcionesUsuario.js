$(document).ready(function() {
    var tipoEvento = ((document.ontouchstart !== null) ? 'click' : 'touchstart');
    $("#enviar_datos").click(function(e) {
        e.preventDefault();
        var user = $("#user_name").val();
        var doc = $("#doc_user").val();
        var nombre = $("#nombre").val();
        var apellido = $("#apellido").val();
        var telefono = $("#telefono").val();
        var correo = $("#correo").val();
        var perfil = $("#perfil").val();
        var con = $("#password").val();
        var conf = $("#conf_password").val();
        if (user == "" || doc == "" || nombre == "" || perfil == "" || con == "" || conf == "") {
            e.preventDefault();
            ohSnap("Favor completar todos los campos!", {
                color: "red",
                'duration': '1000'
            });
        } else {
            validarUsuario(user);
        }
    });
    $("#user_name").keyup(function() {
        $(".tooltip").hide();
        $("#user_name").removeClass('border border-danger');
    });
    $("#doc_user").keyup(function() {
        $(".tooltip").hide();
        $("#doc_user").removeClass('border border-danger');
    });
    $(".inactivar_user").on(tipoEvento, function() {
        var id = $(this).attr('id');
        inactivarUsuario(id);
    });
    $(".activar_user").on(tipoEvento, function() {
        var id = $(this).attr('id');
        activarUsuario(id);
    });
    $(".eliminar_user").on(tipoEvento, function() {
        var id = $(this).attr('id');
        eliminarUsuario(id);
    });
    /*-------------------------*/
    $(".restablecer_pass").on(tipoEvento, function() {
        var id = $(this).attr('data-log');
        restablecerPass(id);
    });
    /*-------------------------*/
    function restablecerPass(id) {
        try {
            $.ajax({
                url: '../vistas/ajax/usuarios/restablecer.php',
                method: 'POST',
                data: {
                    'id': id
                },
                cache: false,
                success: function(resultado) {
                    if (resultado == true) {
                        ohSnap("Contraseña restablecida!", {
                            color: "green",
                            "duration": "1000"
                        });
                    } else {
                        ohSnap("Error al restablecer!", {
                            color: "red",
                            "duration": "1000"
                        });
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }

    function validarUsuario(user) {
        try {
            $.ajax({
                url: '../vistas/ajax/usuarios/validarUsuario.php',
                method: 'POST',
                data: {
                    'usuario': user
                },
                cache: false,
                success: function(resultado) {
                    if (resultado == 'ok') {
                        $(".tooltip").show();
                        $("#user_name").focus();
                        $("#user_name").attr('data-toggle', 'tooltip');
                        $("#user_name").addClass('border border-danger');
                        $("#user_name").tooltip({
                            title: "Usuario ya existe",
                            trigger: "focus",
                            placement: "right"
                        });
                        $("#user_name").tooltip('show');
                    } else {
                        $("#form_enviar").submit();
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }

    function inactivarUsuario(id) {
        try {
            $.ajax({
                url: '../vistas/ajax/usuarios/inactivarUsuario.php',
                method: 'POST',
                data: {
                    'id_user': id
                },
                cache: false,
                success: function(resultado) {
                    if (resultado == 'ok') {
                        $('#' + id).removeAttr('title');
                        $('#' + id + '').removeClass('btn btn-danger btn-sm inactivar_user').addClass('btn btn-success btn-sm activar_user');
                        $('#' + id + ' i').removeClass('fa-times').addClass('fa-check');
                        ohSnap("Inactivado correctamente!", {
                            color: "yellow",
                            'duration': '1000'
                        });
                        setTimeout(recargarPagina, 1050);
                    } else {
                        ohSnap("ha ocurrido un error!", {
                            color: "red",
                            'duration': '1000'
                        });
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }

    function activarUsuario(id) {
        try {
            $.ajax({
                url: '../vistas/ajax/usuarios/activarUsuario.php',
                method: 'POST',
                data: {
                    'id_user': id
                },
                cache: false,
                success: function(resultado) {
                    if (resultado == 'ok') {
                        $('#' + id + '').removeAttr('title');
                        $('#' + id + '').removeClass('btn btn-success btn-sm activar_user').addClass('btn btn-danger btn-sm inactivar_user');
                        $('#' + id + ' i').removeClass('fa-times').addClass('fa-times');
                        ohSnap("Activado correctamente!", {
                            color: "yellow",
                            'duration': '1000'
                        });
                        setTimeout(recargarPagina, 1050);
                    } else {
                        ohSnap("ha ocurrido un error!", {
                            color: "red",
                            'duration': '1000'
                        });
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }

    function eliminarUsuario(id) {
        try {
            $.ajax({
                url: '../vistas/ajax/usuarios/eliminarUsuario.php',
                method: 'POST',
                data: {
                    'id_user': id
                },
                cache: false,
                success: function(resultado) {
                    if (resultado == 'ok') {
                        ohSnap("Eliminado correctamente!", {
                            color: "green",
                            'duration': '1000'
                        });
                        $('#usuario' + id).fadeOut();
                    } else {
                        ohSnap("ha ocurrido un error!", {
                            color: "red",
                            'duration': '1000'
                        });
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }

    function recargarPagina() {
        window.location.replace('index');
    }
});