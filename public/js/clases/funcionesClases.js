$(document).ready(function() {
    var tipoEvento = ((document.ontouchstart !== null) ? 'click' : 'touchstart');
    /*--------------------------*/
    $(".fecha_inicio").change(function() {
        var fecha = $(this).val();
        calcularFinSemana(fecha);
    });
    /*----------------------------*/
    function calcularFinSemana(fecha) {
        try {
            $.ajax({
                url: '../vistas/ajax/clases/calcularFinSemana.php',
                method: 'POST',
                data: {
                    'fecha': fecha
                },
                cache: false,
                success: function(resultado) {
                    $(".fecha_fin").val(resultado);
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }
});