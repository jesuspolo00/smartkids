<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
include_once CONTROL_PATH . 'usuario' . DS . 'ControlUsuario.php';
include_once CONTROL_PATH . 'perfil' . DS . 'ControlPerfil.php';

$instancia        = ControlUsuario::singleton_usuario();
$instancia_perfil = ControlPerfil::singleton_perfil();

$datos_perfil       = $instancia_perfil->mostrarPerfilesControl();
$datos_usuarios     = $instancia->mostrarDatosUsuariosControl();
$usuarios_activos   = $instancia->contarUsuariosActivosControl();
$usuarios_inactivos = $instancia->contarUsuariosInactivosControl();
$usuarios_totales   = $instancia->contarUsuariosTotalesControl();

$permisos = $instancia_permiso->permisosUsuarioControl(2, $perfil_log);

if (!$permisos) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow mb-4">
				<!-- Card Header - Dropdown -->
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-purple">
						<a href="<?=BASE_URL?>configuracion/index" class="text-decoration-none">
							<i class="fa fa-arrow-left text-purple"></i>
						</a>
						&nbsp;
						Estudiantes
					</h4>
					<div class="btn-group">
						<button class="btn btn-purple btn-sm" type="button" data-toggle="modal" data-target="#agregar_usuario">
							<i class="fa fa-plus"></i>
							&nbsp;
							Agregar Usuario
						</button>
					</div>
				</div>
				<div class="card-body">
					<form method="POST">
						<div class="row">
							<div class="col-lg-8 form-inline">
							</div>
							<div class="col-lg-4 form-group">
								<div class="input-group mb-3">
									<input type="text" class="form-control" placeholder="Buscar..." data-tooltip="tooltip" title="Presiona ENTER para buscar" data-placement="top" data-trigger="focus" name="buscar">
									<div class="input-group-append">
										<button class="btn btn-purple btn-sm" type="submit">
											<i class="fa fa-search"></i>
											&nbsp;
											Buscar
										</button>
									</div>
								</div>
							</div>
						</div>
					</form>
					<div class="table-responsive">
						<table class="table table-hover border table-sm" width="100%" cellspacing="0">
							<thead>
								<tr class="text-center font-weight-bold">
									<th scope="col">Documento</th>
									<th scope="col">Nombre completo</th>
									<th scope="col">Acudiente</th>
									<th scope="col">Direccion</th>
									<th scope="col">Correo</th>
									<th scope="col">Telefono</th>
									<th scope="col">Usuario</th>
								</tr>
							</thead>
							<tbody class="buscar">
								<?php
								foreach ($datos_usuarios as $datos) {
									$id_user        = $datos['id_user'];
									$documento      = $datos['documento'];
									$nombre         = $datos['nombre'];
									$apellido       = $datos['apellido'];
									$correo         = $datos['correo'];
									$telefono       = $datos['telefono'];
									$user           = $datos['user'];
									$estado         = $datos['activo'];
									$id_perfil_user = $datos['perfil'];
									$pass_old       = $datos['pass'];
									$id_rol         = $datos['perfil'];
									$acudiente      = $datos['acudiente'];
									$direccion      = $datos['direccion'];

									if ($estado == 1) {
										$title = 'Inactivar';
										$icon  = '<i class="fa fa-times"></i>';
										$class = 'btn-danger inactivar_user';
									} else {
										$title = 'Activar';
										$icon  = '<i class="fa fa-check"></i>';
										$class = 'btn-success activar_user';
									}

									$perfil_user = $instancia_perfil->mostrarDatosPerfilControl($id_user);
									$perfil      = $perfil_user['nom_perfil'];

									$ver_eliminar = ($perfil_log == 1) ? '' : 'd-none';

									if ($datos['perfil'] == 4) {

										?>
										<tr class="text-center" id="usuario<?=$id_user;?>">
											<td><?=$documento?></td>
											<td class="text-uppercase"><?=$nombre . ' ' . $apellido?></td>
											<td><?=$acudiente?></td>
											<td><?=$direccion?></td>
											<td><?=$correo?></td>
											<td><?=$telefono?></td>
											<td><?=$user?></td>
											<td>
												<div class="btn-group" role="group" aria-label="Basic example">
													<button class="btn btn-sm btn-purple editar_user" data-toggle="modal" data-tooltip="tooltip" data-placement="bottom" data-target="#editar_user<?=$id_user?>" title="Editar Usuario" data-trigger="hover">
														<i class="fa fa-user-edit"></i>
													</button>
													<button class="btn btn-sm <?=$class?>" data-tooltip="tooltip" data-placement="bottom" title="<?=$title?>" data-trigger="hover" id="<?=$id_user?>">
														<?=$icon?>
													</button>
												</div>
											</td>
										</tr>

										<div class="modal fade" id="editar_user<?=$id_user?>" tabindex="-1" role="dialog" aria-hidden="true" aria-labelledby="exampleModalLabel">
											<div class="modal-dialog modal-lg p-2" role="document">
												<div class="modal-content">
													<form method="POST">
														<input type="hidden" name="id_user" value="<?=$id_user?>">
														<input type="hidden" name="url" value="1">
														<div class="modal-header p-3">
															<h4 class="modal-title text-purple font-weight-bold">Editar Usuario</h4>
														</div>
														<div class="modal-body border-0">
															<div class="row  p-3">
																<div class="col-lg-6">
																	<div class="form-group">
																		<label class="font-weight-bold">Documento <span class="text-danger">*</span></label>
																		<input type="text" readonly class="form-control numeros" maxlength="50" minlength="1" name="documento_edit" value="<?=$documento?>" required>
																	</div>
																</div>
																<div class="col-lg-6">
																	<div class="form-group">
																		<label class="font-weight-bold">Nombre <span class="text-danger">*</span></label>
																		<input type="text" class="form-control letras" maxlength="50" minlength="1" name="nombre_edit" required value="<?=$nombre?>">
																	</div>
																</div>
																<div class="col-lg-6">
																	<div class="form-group">
																		<label class="font-weight-bold">Apellido</label>
																		<input type="text" class="form-control letras" maxlength="50" minlength="1" name="apellido_edit" value="<?=$apellido?>">
																	</div>
																</div>
																<div class="col-lg-6">
																	<div class="form-group">
																		<label class="font-weight-bold">Telefono</label>
																		<input type="text" class="form-control numeros" maxlength="50" minlength="1" name="telefono_edit" value="<?=$telefono?>">
																	</div>
																</div>
																<div class="col-lg-6">
																	<div class="form-group">
																		<label class="font-weight-bold">Correo</label>
																		<input type="email" class="form-control" name="correo_edit" value="<?=$correo?>">
																	</div>
																</div>
																<div class="col-lg-6">
																	<div class="form-group">
																		<label class="font-weight-bold">Acudiente <span class="text-danger">*</span></label>
																		<input type="text" class="form-control" name="acudiente" value="<?=$acudiente?>">
																	</div>
																</div>
																<div class="col-lg-6">
																	<div class="form-group">
																		<label class="font-weight-bold">Direccion <span class="text-danger">*</span></label>
																		<input type="text" class="form-control" name="direccion" value="<?=$direccion?>">
																	</div>
																</div>
																<div class="col-lg-6">
																	<div class="form-group">
																		<label class="font-weight-bold">Usuario <span class="text-danger">*</span></label>
																		<input type="text" class="form-control" maxlength="100" minlength="1" name="usuario_edit" required readonly value="<?=$user?>">
																	</div>
																</div>
																<div class="col-lg-6">
																	<div class="form-group">
																		<label class="font-weight-bold">Perfil <span class="text-danger">*</span></label>
																		<select class="form-control" name="perfil_edit" required>
																			<option selected value="<?=$id_perfil_user?>" class="d-none"><?=$perfil?></option>
																			<?php
																			foreach ($datos_perfil as $perfiles) {
																				$id_perfil  = $perfiles['id_perfil'];
																				$nom_perfil = $perfiles['nombre'];
																				?>
																				<option value="<?=$id_perfil?>"><?=$nom_perfil?></option>
																				<?php
																			}
																			?>
																		</select>
																	</div>
																</div>
															</div>
															<div class="col-lg-12 form-group mt-2">
																<div class="row">
																	<div class="col-lg-6 form-group text-left">
																		<button class="btn btn-secondary btn-sm restablecer_pass" type="button" data-log="<?=$id_user?>">
																			<i class="fas fa-sync"></i>
																			&nbsp;
																			Restablecer Contrase&ntilde;a
																		</button>
																	</div>
																	<div class="col-lg-6 form-group text-right">
																		<button class="btn btn-danger btn-sm" type="button" data-dismiss="modal">
																			<i class="fa fa-times"></i>
																			&nbsp;
																			Cancelar
																		</button>
																		<button class="btn btn-purple btn-sm" type="submit">
																			<i class="fa fa-edit"></i>
																			&nbsp;
																			Editar
																		</button>
																	</div>
																</div>
															</div>
														</div>
													</form>
												</div>
											</div>
										</div>

										<?php
									}
								}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
include_once VISTA_PATH . 'modulos' . DS . 'usuarios' . DS . 'agregarUsuario.php';
if (isset($_POST['documento'])) {
	$instancia->registrarUsuarioControl();
}

if (isset($_POST['nombre_edit'])) {
	$instancia->editarUsuarioControl();
}
?>
<script src="<?=PUBLIC_PATH?>js/usuario/funcionesUsuario.js"></script>
<script type="text/javascript" src="<?=PUBLIC_PATH?>js/validaciones.js"></script>