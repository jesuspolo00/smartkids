<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
include_once CONTROL_PATH . 'clases' . DS . 'ControlClases.php';

$instancia = ControlClases::singleton_clases();

if (isset($_POST['fecha_fin'])) {
	$fecha_inicio = $_POST['fecha_inicio'];
	$fecha_fin    = $_POST['fecha_fin'];

	$datos_clases = $instancia->mostrarClasesFechaControl($fecha_inicio, $fecha_fin);
} else {
	$datos_clases = $instancia->mostrarClasesControl();
}

?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow mb-4">
				<!-- Card Header - Dropdown -->
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-purple">
						Clases programadas
					</h4>
				</div>
				<div class="card-body">
					<form method="POST">
						<div class="row">
							<div class="col-lg-4 form-group">
								<input type="date" class="form-control filtro_change" data-tooltip="tooltip" title="Fecha inicio" data-trigger="hover" data-placement="bottom" name="fecha_inicio">
							</div>
							<div class="col-lg-4 form-group">
								<input type="date" class="form-control filtro_change" data-tooltip="tooltip" title="Fecha fin" data-trigger="hover" data-placement="bottom" name="fecha_fin" value="<?=date('Y-m-d')?>">
							</div>
							<div class="col-lg-4 form-group">
								<div class="input-group mb-3">
									<input type="text" class="form-control" placeholder="Buscar..." aria-label="Recipient's username" aria-describedby="basic-addon2">
									<div class="input-group-append">
										<button class="btn btn-purple btn-sm" type="submit">
											<i class="fa fa-search"></i>
											&nbsp;
											Buscar
										</button>
									</div>
								</div>
							</div>
						</div>
					</form>
					<div class="table-responsive">
						<table class="table table-hover border table-sm" width="100%" cellspacing="0">
							<thead>
								<tr class="text-center font-weight-bold">
									<th></th>
									<th scope="col">Estudiante</th>
									<th scope="col">Fecha Inicio (Semana)</th>
									<th scope="col">Fecha Fin (Semana)</th>
									<th scope="col">Dia</th>
									<th scope="col">Horas</th>
									<th scope="col">Observacion</th>
								</tr>
							</thead>
							<tbody class="buscar">
								<?php
								foreach ($datos_clases as $clase) {
									$id_clase       = $clase['id'];
									$nom_profesor   = $clase['nom_profesor'];
									$nom_estudiante = $clase['nom_estudiante'];
									$horas          = $clase['horas'];
									$fecha_inicio   = $clase['fecha_inicio'];
									$fecha_fin      = $clase['fecha_fin'];
									$estado         = $clase['estado'];
									$observacion    = $clase['observacion'];
									$id_profesor    = $clase['id_profesor'];
									$id_estudiante  = $clase['id_estudiante'];

									$foto_perfil_user = ($clase['foto_estudiante'] == '') ? 'img/user.svg' : 'upload/' . $clase['foto_estudiante'];

									$dias = array('Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado');
									$dia  = $dias[$clase['dia']];

									$span      = '';
									$ver_boton = '';

									if ($estado == 0) {
										$span      = '<span class="badge badge-warning">Pendiente</span>';
										$ver_boton = '';
									}
									if ($estado == 1) {
										$span      = '<span class="badge badge-success">Vista</span>';
										$ver_boton = 'd-none';
									}
									if ($estado == 2) {
										$span      = '<span class="badge badge-danger">No vista</span>';
										$ver_boton = 'd-none';
									}

									if ($estado == 3) {
										$ver_boton = 'd-none';
										$span      = '<span class="badge badge-secondary">Aprobado</span>';
									}

									$inicio_semana = date("Y-m-d", strtotime('monday this week', strtotime(date('Y-m-d'))));
									$fin_semana    = date("Y-m-d", strtotime('sunday this week', strtotime(date('Y-m-d'))));

									if ($fecha_fin == $fin_semana && date('w') < $clase['dia'] && $estado == 0) {
										$span      = '<span class="badge badge-warning">Pendiente</span>';
										$ver_boton = 'd-none';
									}

									if ($fecha_fin == $fin_semana && $estado == 0 && date('w') == $clase['dia']) {
										$span      = '<span class="badge badge-secondary">Plazo hasta hoy</span>';
										$ver_boton = '';
									}

									if ($fecha_fin == $fin_semana && $estado == 0 && date('w') > $clase['dia']) {
										$span      = '<span class="badge badge-danger">No vista</span>';
										$ver_boton = '';
									}

									if ($clase['id_profesor'] == $id_log) {
										?>
										<tr class="text-center">
											<td>
												<button class="btn btn-primary btn-sm" type="button" data-tooltip="tooltip" title="Foto Estudiante" data-placement="bottom" data-toggle="modal" data-target="#foto_<?=$id_estudiante?>">
													<i class="fas fa-image"></i>
												</button>
											</td>
											<td><?=$nom_estudiante?></td>
											<td><?=$fecha_inicio?></td>
											<td><?=$fecha_fin?></td>
											<td><?=$dia?></td>
											<td><?=$horas?></td>
											<td><?=$observacion?></td>
											<td>
												<?=$span?>
											</td>
											<td>
												<div class="btn-group">
													<button class="btn btn-success btn-sm <?=$ver_boton?>" data-tooltip="tooltip" title="Confirmar clase" data-trigger="hover" data-placement="bottom" data-toggle="modal" data-target="#confirmar<?=$id_clase?>">
														<i class="fa fa-check"></i>
													</button>
												</div>
											</td>
										</tr>


										<div class="modal fade" id="confirmar<?=$id_clase?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
											<div class="modal-dialog modal-lg" role="document">
												<div class="modal-content">
													<div class="modal-header">
														<h5 class="modal-title text-purple font-weight-bold" id="exampleModalLabel">Confirmar clase (#<?=$id_clase?>)</h5>
													</div>
													<form method="POST" enctype="multipart/form-data">
														<input type="hidden" name="id_clase" value="<?=$id_clase?>">
														<div class="modal-body">
															<div class="row p-2">
																<div class="col-lg-6 form-group">
																	<label class="font-weight-bold">Estudiante <span class="text-danger">*</span></label>
																	<input type="text" class="form-control" value="<?=$nom_estudiante?>" disabled>
																</div>
																<div class="col-lg-6 form-group">
																	<label class="font-weight-bold">Horas <span class="text-danger">*</span></label>
																	<input type="text" class="form-control" value="<?=$horas?>" disabled>
																</div>
																<div class="col-lg-6 form-group">
																	<label class="font-weight-bold">Fecha incio (semana)<span class="text-danger">*</span></label>
																	<input type="text" class="form-control" value="<?=$fecha_inicio?>" disabled>
																</div>
																<div class="col-lg-6 form-group">
																	<label class="font-weight-bold">Fecha fin (semana)<span class="text-danger">*</span></label>
																	<input type="text" class="form-control" value="<?=$fecha_fin?>" disabled>
																</div>
																<div class="col-lg-6 form-group">
																	<label class="font-weight-bold">Dia (semana)<span class="text-danger">*</span></label>
																	<input type="text" class="form-control" value="<?=$dia?>" disabled>
																</div>
																<div class="col-lg-6 form-group">
																	<label class="font-weight-bold">Estado <span class="text-danger">*</span></label>
																	<select name="estado" class="form-control" required>
																		<option value="" selected>Seleccione una opcion...</option>
																		<option value="1">Vista</option>
																		<option value="2">No vista</option>
																	</select>
																</div>
																<div class="col-lg-6 form-group">
																	<label class="font-weight-bold">Evidencia</label>
																	<div class="custom-file pmd-custom-file-filled">
																		<input type="file" class="custom-file-input file_input" id="<?=$id_clase?>" name="evidencia" accept=".png, .jpg, .jpeg">
																		<label class="custom-file-label file_label_<?=$id_clase?>" for="customfilledFile"></label>
																	</div>
																</div>
																<div class="col-lg-12 form-group">
																	<label class="font-weight-bold">Observacion</label>
																	<textarea class="form-control" maxlength="1300" name="observacion" rows="5"></textarea>
																</div>
															</div>
															<div class="col-lg-12 form-group mt-2 text-right">
																<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
																	<i class="fa fa-times"></i>
																	&nbsp;
																	Cerrar
																</button>
																<button type="submit" class="btn btn-purple btn-sm">
																	<i class="fa fa-edit"></i>
																	&nbsp;
																	Confirmar
																</button>
															</div>
														</div>
													</form>
												</div>
											</div>
										</div>


										<div class="modal fade" id="foto_<?=$id_estudiante?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
											<div class="modal-dialog modal-lg">
												<div class="modal-content">
													<div class="modal-header">
														<h5 class="modal-title font-weight-bold text-purple" id="exampleModalLabel">Foto Profesor</h5>
														<button type="button" class="btn btn-sm" data-dismiss="modal" aria-label="Close">
															<i class="fa fa-times"></i>
														</button>
													</div>
													<div class="modal-body">
														<div class="row">
															<div class="col-lg-12 form-group">
																<div class="circular--portrait">
																	<img src="<?=PUBLIC_PATH?><?=$foto_perfil_user?>">
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<?php
									}
								}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';

if (isset($_POST['id_clase'])) {
	$instancia->confirmarClaseControl();
}