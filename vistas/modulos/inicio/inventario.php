<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
include_once CONTROL_PATH . 'inventario' . DS . 'ControlInventario.php';

$instancia = ControlInventario::singleton_inventario();

$buscar = $instancia->mostrarDatosInventarioControl();

?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow mb-4">
				<!-- Card Header - Dropdown -->
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-purple">
						Inventario
					</h4>
				</div>
				<div class="card-body">
					<form method="POST">
						<div class="row">
							<div class="col-lg-8"></div>
							<div class="col-lg-4">
								<div class="input-group mb-3 input-sm">
									<input type="text" class="form-control" placeholder="Buscar" aria-label="Recipient's username" aria-describedby="basic-addon2">
									<div class="input-group-append">
										<span class="input-group-text" id="basic-addon2">
											<i class="fa fa-search"></i>
										</span>
									</div>
								</div>
							</div>
						</div>
					</form>
					<div class="table-responsive">
						<table class="table table-hover border table-sm" width="100%" cellspacing="0">
							<thead>
								<tr class="text-center font-weight-bold">
									<th scope="col">#</th>
									<th scope="col">Profesor</th>
									<th scope="col">Articulo</th>
									<th scope="col">Cantidad</th>
									<th scope="col">Estado</th>
								</tr>
							</thead>
							<tbody class="buscar">
								<?php
								foreach ($buscar as $articulo) {
									$id_articulo = $articulo['id'];
									$descripcion = $articulo['descripcion'];
									$cantidad    = $articulo['cantidad'];
									$profesor    = $articulo['nom_usuario'];
									$estado      = $articulo['estado'];

									$ver_boton = '';

									if ($estado == 0) {
										$span = '<span class="badge badge-secondary">Nuevo</span>';
									}

									if ($estado == 1 || $cantidad == 0) {
										$span      = '<span class="badge badge-danger">Agotado</span>';
										$ver_boton = 'd-none';
									}

									if ($estado == 3) {
										$span      = '<span class="badge badge-warning">Pendiente de confirmacion</span>';
										$ver_boton = 'd-none';
									}

									if ($estado == 4) {
										$span      = '<span class="badge badge-success">Aprobado</span>';
										$ver_boton = '';
									}

									if ($articulo['id_user'] == $id_log) {
										?>
										<tr class="text-center">
											<td><?=$id_articulo?></td>
											<td><?=$profesor?></td>
											<td><?=$descripcion?></td>
											<td><?=$cantidad?></td>
											<td>
												<?=$span?>
											</td>
											<td>
												<div class="btn-group" role="group" aria-label="Basic example">
													<button class="btn btn-success btn-sm" data-tooltip="tooltip" title="Solicitar articulo" data-toggle="modal" data-target="#soli<?=$id_articulo?>">
														<i class="fas fa-plus"></i>
													</button>
													<button class="btn btn-danger btn-sm <?=$ver_boton?>" data-tooltip="tooltip" title="Disminuir cantidad" data-trigger="hover" data-placement="bottom" data-toggle="modal" data-target="#dismi<?=$id_articulo?>">
														<i class="fa fa-minus"></i>
													</button>
												</div>
											</td>
										</tr>


										<!-- Modal -->
										<div class="modal fade" id="soli<?=$id_articulo?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
											<div class="modal-dialog modal-lg" role="document">
												<div class="modal-content">
													<div class="modal-header">
														<h5 class="modal-title font-weight-bold text-purple" id="exampleModalLabel">Solicitar articulo</h5>
													</div>
													<form method="POST">
														<input type="hidden" name="id_log" value="<?=$id_log?>">
														<input type="hidden" name="id_articulo" value="<?=$id_articulo?>">
														<input type="hidden" name="estado" value="3">
														<input type="hidden" name="inicio" value="1">
														<div class="modal-body">
															<div class="row p-2">
																<div class="col-lg-6 form-group">
																	<label class="font-weight-bold">Articulo</label>
																	<input type="text" class="form-control" value="<?=$descripcion?>" disabled>
																</div>
																<div class="col-lg-6 form-group">
																	<label class="font-weight-bold">Profesor</label>
																	<input type="text" class="form-control" value="<?=$profesor?>" disabled>
																</div>
																<div class="col-lg-6 form-group">
																	<label class="font-weight-bold">Cantidad</label>
																	<input type="text" class="form-control" value="<?=$cantidad?>" readonly name="cant_actual">
																</div>
																<div class="col-lg-6 form-group">
																	<label class="font-weight-bold">Cantidad a solicitar <span class="text-danger">*</span></label>
																	<input type="text" name="cant_dism" required class="form-control numeros" value="0">
																</div>
															</div>
														</div>
														<div class="modal-footer border-0">
															<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
																<i class="fa fa-times"></i>
																&nbsp;
																Cerrar
															</button>
															<button type="submit" class="btn btn-success btn-sm">
																<i class="fa fa-save"></i>
																&nbsp;
																Guardar
															</button>
														</div>
													</form>
												</div>
											</div>
										</div>


										<!-- Modal -->
										<div class="modal fade" id="dismi<?=$id_articulo?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
											<div class="modal-dialog modal-lg" role="document">
												<div class="modal-content">
													<div class="modal-header">
														<h5 class="modal-title font-weight-bold text-purple" id="exampleModalLabel">Disminuir articulo</h5>
													</div>
													<form method="POST">
														<input type="hidden" name="id_log" value="<?=$id_log?>">
														<input type="hidden" name="id_articulo" value="<?=$id_articulo?>">
														<input type="hidden" name="estado" value="0">
														<input type="hidden" name="inicio" value="1">
														<div class="modal-body">
															<div class="row p-2">
																<div class="col-lg-6 form-group">
																	<label class="font-weight-bold">Articulo</label>
																	<input type="text" class="form-control" value="<?=$descripcion?>" disabled>
																</div>
																<div class="col-lg-6 form-group">
																	<label class="font-weight-bold">Profesor</label>
																	<input type="text" class="form-control" value="<?=$profesor?>" disabled>
																</div>
																<div class="col-lg-6 form-group">
																	<label class="font-weight-bold">Cantidad</label>
																	<input type="text" class="form-control" value="<?=$cantidad?>" readonly name="cant_actual">
																</div>
																<div class="col-lg-6 form-group">
																	<label class="font-weight-bold">Cantidad a disminuir (cantidad maxima <?=$cantidad?>) <span class="text-danger">*</span></label>
																	<input type="text" name="cant_dism" required class="form-control numeros" value="0">
																</div>
															</div>
														</div>
														<div class="modal-footer border-0">
															<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
																<i class="fa fa-times"></i>
																&nbsp;
																Cerrar
															</button>
															<button type="submit" class="btn btn-success btn-sm">
																<i class="fa fa-save"></i>
																&nbsp;
																Guardar
															</button>
														</div>
													</form>
												</div>
											</div>
										</div>

										<?php
									}
								}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';

if (isset($_POST['descripcion'])) {
	$instancia->registrarInventarioControl();
}

if (isset($_POST['id_articulo'])) {
	$instancia->estadoArticuloControl();
}
?>