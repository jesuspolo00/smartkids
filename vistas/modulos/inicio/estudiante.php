<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
$logi="";
$time= date('Y-m-d h:iA',time());
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	
	header('Location:../login?er=' . $error);
	exit();
}

include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
include_once CONTROL_PATH . 'clases' . DS . 'ControlClases.php';

$instancia = ControlClases::singleton_clases();

$datos_clases = $instancia->mostrarClasesControl();
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow mb-4">
				<!-- Card Header - Dropdown -->
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-purple">
						Mis clases
					</h4>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-lg-8 form-inline">
						</div>
						<div class="col-lg-4">
							<form>
								<div class="form-group">
									<div class="input-group mb-3">
										<input type="text" class="form-control filtro" placeholder="Buscar">
										<div class="input-group-prepend">
											<span class="input-group-text rounded-right" id="basic-addon1">
												<i class="fa fa-search"></i>
											</span>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="table-responsive">
						<table class="table table-hover border table-sm" width="100%" cellspacing="0">
							<thead>
								<tr class="text-center font-weight-bold">
									<th></th>
									<th scope="col">Profesor</th>
									<th scope="col">Fecha inicio (semana)</th>
									<th scope="col">Fecha fin (semana)</th>
									<th scope="col">Dia (semana)</th>
									<th scope="col">Horas al dia</th>
									<th scope="col">Observacion</th>
								</tr>
							</thead>
							<tbody class="buscar">
								<?php
								$logi= "Pudo ingresar al sistema";
								foreach ($datos_clases as $clase) {
									$id_clase       = $clase['id'];
									$nom_profesor   = $clase['nom_profesor'];
									$nom_estudiante = $clase['nom_estudiante'];
									$horas          = $clase['horas'];
									$fecha_inicio   = $clase['fecha_inicio'];
									$fecha_fin      = $clase['fecha_fin'];
									$estado         = $clase['estado'];
									$observacion    = $clase['observacion'];
									$id_profesor    = $clase['id_profesor'];

									$dias = array('Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado');
									$dia  = $dias[$clase['dia']];

									$ver_comentario = '';

									$radio_uno    = rand();
									$radio_dos    = rand();
									$radio_tres   = rand();
									$radio_cuatro = rand();
									$radio_cinco  = rand();

									$foto_perfil_user = ($clase['foto_perfil'] == '') ? 'img/user.svg' : 'upload/' . $clase['foto_perfil'];

									$comentario = $instancia->mostrarComentariosClaseControl($id_log, $id_clase);

									if ($comentario['id'] != '') {
										$ver_comentario = 'd-none';
									}

									if ($estado == 0) {
										$span           = '<span class="badge badge-warning">Pendiente</span>';
										$ver_comentario = 'd-none';
									}
									if ($estado == 3) {
										$span = '<span class="badge badge-success">Clase vista</span>';
										if ($comentario['id'] != '') {
											$ver_comentario = 'd-none';
										}
									}

									$inicio_semana = date("Y-m-d", strtotime('monday this week', strtotime(date('Y-m-d'))));
									$fin_semana    = date("Y-m-d", strtotime('sunday this week', strtotime(date('Y-m-d'))));

									$ver_clase = 'd-none';
									if ($fecha_fin == $fin_semana) {
										$ver_clase = '';
									}

									if ($clase['id_estudiante'] == $id_log && $estado != 2 && $estado != 1) {
										?>
										<tr class="text-center <?=$ver_clase?>">
											<td>
												<button class="btn btn-primary btn-sm" type="button" data-tooltip="tooltip" title="Foto Profesor" data-placement="bottom" data-toggle="modal" data-target="#foto_<?=$id_profesor?>">
													<i class="fas fa-image"></i>
												</button>
											</td>
											<td><?=$nom_profesor?></td>
											<td><?=$fecha_inicio?></td>
											<td><?=$fecha_fin?></td>
											<td><?=$dia?></td>
											<td><?=$horas?></td>
											<td><?=$observacion?></td>
											<td>
												<?=$span?>
											</td>
											<td>
												<button class="btn btn-success btn-sm <?=$ver_comentario?>" data-tooltip="tooltip" title="Rese&ntilde;a" data-trigger="hover" data-placement="bottom" data-toggle="modal" data-target="#comentar<?=$id_clase?>">
													<i class="fas fa-comments"></i>
												</button>
												<?php
												if ($comentario['id'] != '') {
													for ($i = 0; $i < $comentario['puntuacion']; $i++) {
														?>
														<i class="fas fa-star text-warning"></i>
														<?php
													}
												}
												?>
											</td>

											<div class="modal fade" id="comentar<?=$id_clase?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
												<div class="modal-dialog modal-lg" role="document">
													<div class="modal-content">
														<div class="modal-header">
															<h5 class="modal-title text-purple font-weight-bold" id="exampleModalLabel">Comentar clase (#<?=$id_clase?>)</h5>
														</div>
														<form method="POST">
															<input type="hidden" name="id_clase" value="<?=$id_clase?>">
															<input type="hidden" name="id_log" value="<?=$id_log?>">
															<input type="hidden" name="id_profesor" value="<?=$clase['id_profesor']?>">
															<div class="modal-body">
																<div class="row p-2">
																	<div class="col-lg-12 text-center mb-4">
																		<img src="<?=PUBLIC_PATH . $foto_perfil_user?>" class="rounded img-profile" width="150" height="150" alt="...">
																	</div>
																	<div class="col-lg-6 form-group">
																		<label class="font-weight-bold">Profesor <span class="text-danger">*</span></label>
																		<input type="text" class="form-control" value="<?=$nom_profesor?>" disabled>
																	</div>
																	<div class="col-lg-6 form-group">
																		<label class="font-weight-bold">Horas <span class="text-danger">*</span></label>
																		<input type="text" class="form-control" value="<?=$horas?>" disabled>
																	</div>
																	<div class="col-lg-6 form-group">
																		<label class="font-weight-bold">Fecha incio (semana)<span class="text-danger">*</span></label>
																		<input type="text" class="form-control" value="<?=$fecha_inicio?>" disabled>
																	</div>
																	<div class="col-lg-6 form-group">
																		<label class="font-weight-bold">Fecha fin (semana)<span class="text-danger">*</span></label>
																		<input type="text" class="form-control" value="<?=$fecha_fin?>" disabled>
																	</div>
																	<div class="col-lg-6 form-group">
																		<label class="font-weight-bold">Dia (semana)<span class="text-danger">*</span></label>
																		<input type="text" class="form-control" value="<?=$dia?>" disabled>
																	</div>
																	<div class="col-lg-12 form-group">
																		<label class="font-weight-bold">Evidencia</label>
																		<br>
																		<img src="<?=PUBLIC_PATH?>upload/<?=$clase['evidencia']?>" style="width: 100%;" alt="">
																	</div>
																	<div class="col-lg-12 form-group">
																		<label class="font-weight-bold">Observacion</label>
																		<textarea disabled rows="5" class="form-control"><?=$observacion?></textarea>
																	</div>
																	<div class="col-lg-12 form-group mt-4">
																		<label class="font-weight-bold">Puntuar y escribir rese&ntilde;a</label>
																		<div class="col-lg-12 text-center form-group">
																			<p class="clasificacion">
																				<input id="radio<?=$radio_uno?>" type="radio" name="estrellas" required value="5">
																				<label class="ml-3" for="radio<?=$radio_uno?>">
																					<i class="fas fa-star fa-3x"></i>
																				</label>
																				<input id="radio<?=$radio_dos?>" type="radio" name="estrellas" required value="4">
																				<label class="ml-3" for="radio<?=$radio_dos?>">
																					<i class="fas fa-star fa-3x"></i>
																				</label>
																				<input id="radio<?=$radio_tres?>" type="radio" name="estrellas" required value="3">
																				<label class="ml-3" for="radio<?=$radio_tres?>">
																					<i class="fas fa-star fa-3x"></i>
																				</label>
																				<input id="radio<?=$radio_cuatro?>" type="radio" name="estrellas" required value="2">
																				<label class="ml-3" for="radio<?=$radio_cuatro?>">
																					<i class="fas fa-star fa-3x"></i>
																				</label>
																				<input id="radio<?=$radio_cinco?>" type="radio" name="estrellas" required value="1">
																				<label class="ml-3" for="radio<?=$radio_cinco?>">
																					<i class="fas fa-star fa-3x"></i>
																				</label>
																			</p>
																		</div>
																		<textarea class="form-control" maxlength="1300" name="comentario" placeholder="Describe tu experiencia (opcional)" rows="5"></textarea>
																	</div>
																</div>
															</div>
															<div class="modal-footer border-0">
																<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
																	<i class="fa fa-times"></i>
																	&nbsp;
																	Cerrar
																</button>
																<button type="submit" class="btn btn-success btn-sm">
																	<i class="fa fa-comments"></i>
																	&nbsp;
																	Comentar
																</button>
															</div>
														</form>
													</div>
												</div>
											</div>
										</tr>


										<div class="modal fade" id="foto_<?=$id_profesor?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
											<div class="modal-dialog modal-lg">
												<div class="modal-content">
													<div class="modal-header">
														<h5 class="modal-title font-weight-bold text-purple" id="exampleModalLabel">Foto Profesor</h5>
														<button type="button" class="btn btn-sm" data-dismiss="modal" aria-label="Close">
															<i class="fa fa-times"></i>
														</button>
													</div>
													<div class="modal-body">
														<div class="row">
															<div class="col-lg-12 form-group">
																<div class="circular--portrait">
																	<img src="<?=PUBLIC_PATH?><?=$foto_perfil_user?>">
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>

										<?php
									}
								}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';

if (isset($_POST['estrellas'])) {
	$instancia->comentarClaseControl();
}