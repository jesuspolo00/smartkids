<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
include_once CONTROL_PATH . 'usuario' . DS . 'ControlUsuario.php';
include_once CONTROL_PATH . 'clases' . DS . 'ControlClases.php';

$instancia       = ControlUsuario::singleton_usuario();
$instancia_clase = ControlClases::singleton_clases();

$datos_usuarios = $instancia->mostrarDatosUsuariosControl();

$permisos = $instancia_permiso->permisosUsuarioControl(2, $perfil_log);

if (!$permisos) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow mb-4">
				<!-- Card Header - Dropdown -->
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-purple">
						Rese&ntilde;as
					</h4>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-lg-8 form-inline">
						</div>
						<div class="col-lg-4">
							<form>
								<div class="form-group">
									<div class="input-group mb-3">
										<input type="text" class="form-control filtro" placeholder="Buscar">
										<div class="input-group-prepend">
											<span class="input-group-text rounded-right" id="basic-addon1">
												<i class="fa fa-search"></i>
											</span>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="table-responsive">
						<table class="table table-hover border table-sm" width="100%" cellspacing="0">
							<thead>
								<tr class="text-center font-weight-bold">
									<th scope="col">#</th>
									<th scope="col">Documento</th>
									<th scope="col">Nombre completo</th>
									<th scope="col">Correo</th>
									<th scope="col">Telefono</th>
								</tr>
							</thead>
							<tbody class="buscar">
								<?php
								foreach ($datos_usuarios as $datos) {
									$id_user        = $datos['id_user'];
									$cedula         = $datos['documento'];
									$nombre         = $datos['nombre'];
									$apellido       = $datos['apellido'];
									$correo         = $datos['correo'];
									$telefono       = $datos['telefono'];
									$user           = $datos['user'];
									$estado         = $datos['activo'];
									$id_perfil_user = $datos['perfil'];
									$pass_old       = $datos['pass'];
									$id_rol         = $datos['perfil'];

									$comentarios = $instancia_clase->comentariosProfesorControl($id_user);

									if ($estado == 1 && $id_perfil_user == 3) {

										?>
										<tr class="text-center">
											<td><?=$id_user?></td>
											<td><?=$cedula?></td>
											<td class="text-uppercase"><?=$nombre . ' ' . $apellido?></td>
											<td><?=$correo?></td>
											<td><?=$telefono?></td>
											<td>
												<div class="btn-group" role="group" aria-label="Basic example">
													<button class="btn btn-info btn-sm" data-tooltip="tooltip" title="Ver comentarios" data-trigger="hover" data-placement="bottom" data-toggle="modal" data-target="#comentarios<?=$id_user?>">
														<i class="fa fa-eye"></i>
													</button>
												</div>
											</td>
										</tr>


										<!-- Modal -->
										<div class="modal fade" id="comentarios<?=$id_user?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
											<div class="modal-dialog modal-lg" role="document">
												<div class="modal-content">
													<div class="modal-header">
														<h5 class="modal-title font-weight-bold text-purple" id="exampleModalLabel">Rese&ntilde;as profesor (<?=$nombre . ' ' . $apellido?>)</h5>
													</div>
													<div class="modal-body">
														<div class="row">
															<?php
															foreach ($comentarios as $dato) {
																$id_coment  = $dato['id'];
																$comentario = $dato['comentario'];
																$puntuacion = $dato['puntuacion'];

																$foto_perfil_estu = ($dato['foto_perfil'] == '') ? 'img/user.svg' : 'upload/' . $dato['foto_perfil'];
																?>
																<div class="col-lg-12 form-group">
																	<div class="card">
																		<div class="card-body">
																			<div class="row">
																				<div class="col-lg-1 form-group">
																					<div class="circular--coment">
																						<img src="<?=PUBLIC_PATH . $foto_perfil_estu?>">
																					</div>
																				</div>
																				<div class="col-lg-10 form-group ml-3 mt-2">
																					<p class="font-weight-bold ml-4"><?=$dato['nom_usuario']?>
																					<span class="float-right">
																						<?php
																						for ($i = 0; $i < $puntuacion; $i++) {
																							?>
																							<i class="fas fa-star text-warning"></i>
																							<?php
																						}
																						?>
																					</span>
																				</p>
																				<p class="h6 ml-4 mt-d1">
																					<?=$comentario?>
																				</p>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														<?php }?>
													</div>
												</div>
											</div>
										</div>
									</div>


									<?php
								}
							}
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
