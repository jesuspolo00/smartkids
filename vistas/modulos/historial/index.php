<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
include_once CONTROL_PATH . 'historial' . DS . 'Controlhistorial.php';


$instancia  = Controlhistorial::singleton_historial();


$datos_usuarios     = $instancia->mostrarDatoshistorialControl();


?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow mb-4">
				<!-- Card Header - Dropdown -->
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-purple">
						<a href="<?=BASE_URL?>configuracion/index" class="text-decoration-none">
							<i class="fa fa-arrow-left text-purple"></i>
						</a>
						&nbsp;
						Historial
					</h4>
					<div class="dropdown no-arrow">
						<a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
						</a>
						<div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
							
					</div>
				</div>
			</div>
								<div class="card-body">
					<form method="POST">
						<div class="row">
							<div class="col-lg-4 form-group">
							</div>
							<div class="col-lg-4 form-group">
							
							</div>
						
						</div>
					</form>
					<div class="table-responsive">
						<table class="table table-hover border table-sm" width="100%" cellspacing="0">
							<thead>
								<tr class="text-center font-weight-bold">
									
									<th scope="col">Estudiante</th>
									<th scope="col">Apellido</th>
									<th scope="col">Fecha ingreso</th>

								
								</tr>
							</thead>
						
						<?php
								foreach ($datos_usuarios as $datos) {
									$id_user  = $datos['nombre'];
									$apellido  = $datos['apellido'];
									$fechareg  = $datos['fecha_reg'];
								

								
									
									 ?>
									 <tr class="text-center">
									 	<td><?=$id_user?></td>
									 	<td><?=$apellido?></td>
									 	<td><?=$fechareg?></td>
									 </tr>
									 <?php
									  }
										?>
						</table>
					</div>
				</div>
			</div>
		</div>
</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
include_once VISTA_PATH . 'modulos' . DS . 'inventario' . DS . 'agregarInventario.php';

if (isset($_POST['descripcion'])) {
	$instancia->registrarInventarioControl();
}

if (isset($_POST['id_articulo'])) {
	$instancia->estadoArticuloControl();
}
?>
<script src="<?=PUBLIC_PATH?>js/inventario/funcionesInventario.js"></script>