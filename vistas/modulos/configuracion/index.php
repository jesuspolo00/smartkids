<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';

$permisos = $instancia_permiso->permisosUsuarioControl(1, $perfil_log);

if (!$permisos) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-purple">Configuracion</h4>
				</div>
				<div class="card-body">
					<div class="row">
						<?php
						$permisos = $instancia_permiso->permisosUsuarioControl(2, $perfil_log);
						if ($permisos) {
							?>
							<a class="col-md-3 mb-4 text-decoration-none" href="<?=BASE_URL?>usuarios/index">
								<div class="card border-left-success shadow-sm h-100 py-2">
									<div class="card-body">
										<div class="row no-gutters align-items-center">
											<div class="col mr-2">
												<div class="h5 mb-0 font-weight-bold text-gray-800">Usuarios</div>
											</div>
											<div class="col-auto">
												<i class="fas fa-users fa-2x text-success"></i>
											</div>
										</div>
									</div>
								</div>
							</a>
						<?php }
						$permisos = $instancia_permiso->permisosUsuarioControl(2, $perfil_log);
						if ($permisos) {
							?>
							<a class="col-md-3 mb-4 text-decoration-none" href="<?=BASE_URL?>usuarios/estudiante">
								<div class="card border-left-info shadow-sm h-100 py-2">
									<div class="card-body">
										<div class="row no-gutters align-items-center">
											<div class="col mr-2">
												<div class="h5 mb-0 font-weight-bold text-gray-800">Estudiantes</div>
											</div>
											<div class="col-auto">
												<i class="fas fa-user-graduate fa-2x text-info"></i>
											</div>
										</div>
									</div>
								</div>
							</a>
						<?php }
						$permisos = $instancia_permiso->permisosUsuarioControl(4, $perfil_log);
						if ($permisos) {
							?>
							<a class="col-md-3 mb-4 text-decoration-none" href="<?=BASE_URL?>inventario/index">
								<div class="card border-left-primary shadow-sm h-100 py-2">
									<div class="card-body">
										<div class="row no-gutters align-items-center">
											<div class="col mr-2">
												<div class="h5 mb-0 font-weight-bold text-gray-800">Inventario</div>
											</div>
											<div class="col-auto">
												<i class="fas fa-cubes fa-2x text-primary"></i>
											</div>
										</div>
									</div>
								</div>
							</a>
						<?php }
								$permisos = $instancia_permiso->permisosUsuarioControl(10, $perfil_log);
						if ($permisos) {
							?>
							<a class="col-md-3 mb-4 text-decoration-none" href="<?=BASE_URL?>historial/index">
								<div class="card border-left-primary shadow-sm h-100 py-2">
									<div class="card-body">
										<div class="row no-gutters align-items-center">
											<div class="col mr-2">
												<div class="h5 mb-0 font-weight-bold text-gray-800">Historial</div>
											</div>
											<div class="col-auto">
												<i class="fas fa-solid fa-2x fa-address-card"></i>
											</div>
										</div>
									</div>
								</div>
							</a>
						<?php }
						$permisos = $instancia_permiso->permisosUsuarioControl(3, $perfil_log);
						if ($permisos) {
							?>
							<a class="col-md-3 mb-4 text-decoration-none" href="<?=BASE_URL?>clases/index">
								<div class="card border-left-warning shadow-sm h-100 py-2">
									<div class="card-body">
										<div class="row no-gutters align-items-center">
											<div class="col mr-2">
												<div class="h5 mb-0 font-weight-bold text-gray-800">Clases</div>
											</div>
											<div class="col-auto">
												<i class="fas fa-calendar-alt fa-2x text-warning"></i>
											</div>
										</div>
									</div>
								</div>
							</a>
						<?php }
						$permisos = $instancia_permiso->permisosUsuarioControl(5, $perfil_log);
						if ($permisos) {
							?>
							<a class="col-md-3 mb-4 text-decoration-none" href="<?=BASE_URL?>permisos/index">
								<div class="card border-left-secondary shadow-sm h-100 py-2">
									<div class="card-body">
										<div class="row no-gutters align-items-center">
											<div class="col mr-2">
												<div class="h5 mb-0 font-weight-bold text-gray-800">Permisos</div>
											</div>
											<div class="col-auto">
												<i class="fas fa-user-lock fa-2x text-secondary"></i>
											</div>
										</div>
									</div>
								</div>
							</a>
						<?php }
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
?>