<?php
require_once CONTROL_PATH . 'ControlSession.php';

$objss = new Session;
$objss->iniciar();
if (!empty($_SESSION['rol'])) {
	header('Location:inicio');
	exit();
}

$ingreso = ingresoClass::singleton_ingreso();
$ingreso->ingresaruser();
$desenc = base64_decode(@$_GET['er']);
if ($err = isset($desenc) ? $desenc : null);
include_once VISTA_PATH . 'cabeza.php';
?>
<div class="container">
	<!-- Outer Row -->
	<div class="row justify-content-center">
		<div class="col-lg-12 mt-15">
			<div class="card o-hidden w-responsive border-0 shadow-sm m-auto">
				<div class="card-body p-0">
					<!-- Nested Row within Card Body -->
					<div class="row">
						<div class="col-lg-10 m-auto">
							<div class="p-5">
								<div class="text-center">
									<h1 class="h4 text-success-900 mb-4">
										<img src="<?=PUBLIC_PATH?>img/logo.png" alt="" class="img-fluid" width="150">
									</h1>
								</div>
								<form class="user" method="POST">
									<div class="form-group">
										<input type="text" class="form-control form-control-user user" maxlength="50" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Usuario" name="user">
									</div>
									<div class="form-group">
										<input type="password" name="pass" class="form-control form-control-user"  id="exampleInputPassword" placeholder="Contrase&ntilde;a">
									</div>
									<input type="submit" class="btn btn-purple btn-user btn-block" value="Ingresar">
									<hr>
								</form>
							</div>
							<?php
							if ($err == 1) {
								echo '<p class="text-danger text-center">Usuario o Contrase&ntilde;a Incorrecta</p>';
							} else if ($err == 2) {
								echo '<p class="text-danger text-center">Debes iniciar sesion para acceder</p>';
							} else if ($err == 3) {
								echo '<p class="text-danger text-center">Usuario o Contrase&ntilde;a Incorrecta</p>';
							} else if ($err == 4) {
								echo '<p class="text-danger text-center">No esta permitido iniciar sesion</p>';
							}
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
?>