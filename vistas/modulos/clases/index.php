<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'usuario' . DS . 'ControlUsuario.php';
require_once CONTROL_PATH . 'clases' . DS . 'ControlClases.php';

$instancia         = ControlClases::singleton_clases();
$instancia_usuario = ControlUsuario::singleton_usuario();

if (isset($_POST['buscar'])) {
	$datos        = array('buscar' => $_POST['buscar'], 'fecha' => $_POST['fecha'], 'profesor' => $_POST['profesor']);
	$datos_clases = $instancia->buscarClasesControl($datos);
} else {
	$datos_clases = $instancia->mostrarClasesControl();
}

$datos_usuarios = $instancia_usuario->mostrarDatosUsuariosControl();

$permisos = $instancia_permiso->permisosUsuarioControl(3, $perfil_log);

if (!$permisos) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow mb-4">
				<!-- Card Header - Dropdown -->
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-purple">
						<a href="<?=BASE_URL?>configuracion/index" class="text-decoration-none">
							<i class="fa fa-arrow-left text-purple"></i>
						</a>
						&nbsp;
						Clases
					</h4>
					<div class="btn-group">
						<button class="btn btn-purple btn-sm" data-toggle="modal" data-target="#agregar_clase" type="button">
							<i class="fa fa-clock"></i>
							&nbsp;
							Programar Clase
						</button>
					</div>
				</div>
				<div class="card-body">
					<form method="POST">
						<div class="row">
							<div class="col-lg-4 form-group">
								<select name="profesor" class="form-control" data-tooltip="tooltip" title="Profesor" data-placement="top">
									<option value="" selected>Seleccione un profesor...</option>
									<?php
									foreach ($datos_usuarios as $usuario) {
										$id_user      = $usuario['id_user'];
										$nom_completo = $usuario['nombre'] . ' ' . $usuario['apellido'];
										$activo       = $usuario['activo'];

										$ver = ($activo == 1 && $usuario['perfil'] == 3) ? '' : 'd-none';
										?>
										<option value="<?=$id_user?>" class="<?=$ver?>"><?=$nom_completo?></option>
										<?php
									}
									?>
								</select>
							</div>
							<div class="col-lg-4 form-group">
								<input type="date" class="form-control" name="fecha" data-tooltip="tooltip" title="Fecha entre semana" data-placement="top">
							</div>
							<div class="col-lg-4 form-group">
								<div class="input-group mb-3">
									<input type="text" class="form-control" placeholder="Buscar..." data-tooltip="tooltip" title="Presiona ENTER para buscar" data-placement="top" data-trigger="focus" name="buscar">
									<div class="input-group-append">
										<button class="btn btn-purple btn-sm" type="submit">
											<i class="fa fa-search"></i>
											&nbsp;
											Buscar
										</button>
									</div>
								</div>
							</div>
						</div>
					</form>
					<div class="table-responsive">
						<table class="table table-hover border table-sm" width="100%" cellspacing="0">
							<thead>
								<tr class="text-center font-weight-bold">
									<th scope="col">No. Clase</th>
									<th scope="col">Profesor</th>
									<th scope="col">Estudiante</th>
									<th scope="col">Fecha Inicio (Semana)</th>
									<th scope="col">Fecha Fin (Semana)</th>
									<th scope="col">Dia</th>
									<th scope="col">Horas</th>
									<th scope="col">Observacion</th>
								</tr>
							</thead>
							<tbody class="buscar">
								<?php
								foreach ($datos_clases as $clase) {
									$id_clase       = $clase['id'];
									$nom_profesor   = $clase['nom_profesor'];
									$nom_estudiante = $clase['nom_estudiante'];
									$horas          = $clase['horas'];
									$fecha_inicio   = $clase['fecha_inicio'];
									$fecha_fin      = $clase['fecha_fin'];
									$estado         = $clase['estado'];
									$observacion    = $clase['observacion'];
									$id_profesor    = $clase['id_profesor'];

									$dias = array('Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado');
									$dia  = $dias[$clase['dia']];

									$ver_aprobado = '';
									$ver_editar   = '';

									if ($estado == 0) {
										$span         = '<span class="badge badge-warning">Pendiente</span>';
										$ver_aprobado = 'd-none';
									}
									if ($estado == 1) {
										$span         = '<span class="badge badge-success">Vista</span>';
										$ver_aprobado = '';
										$ver_editar   = 'd-none';
									}
									if ($estado == 2) {
										$span         = '<span class="badge badge-danger">No vista</span>';
										$ver_aprobado = 'd-none';
										$ver_editar   = 'd-none';
									}

									if ($estado == 3) {
										$span         = '<span class="badge badge-secondary">Aprobada</span>';
										$ver_aprobado = 'd-none';
										$ver_editar   = 'd-none';
									}

									$inicio_semana = date("Y-m-d", strtotime('monday this week', strtotime(date('Y-m-d'))));
									$fin_semana    = date("Y-m-d", strtotime('sunday this week', strtotime(date('Y-m-d'))));

									if ($fecha_fin == $fin_semana && date('w') < $clase['dia'] && $estado == 0) {
										$span      = '<span class="badge badge-warning">Pendiente</span>';
										$ver_boton = 'd-none';
									}

									if ($fecha_fin == $fin_semana && $estado == 0 && date('w') == $clase['dia']) {
										$span      = '<span class="badge badge-secondary">Plazo hasta hoy</span>';
										$ver_boton = '';
									}

									if ($fecha_fin == $fin_semana && $estado == 0 && date('w') > $clase['dia']) {
										$span      = '<span class="badge badge-danger">No vista</span>';
										$ver_boton = 'd-none';
									}

									$checked_0 = ($dia == 'Domingo') ? 'checked' : '';
									$checked_1 = ($dia == 'Lunes') ? 'checked' : '';
									$checked_2 = ($dia == 'Martes') ? 'checked' : '';
									$checked_3 = ($dia == 'Miercoles') ? 'checked' : '';
									$checked_4 = ($dia == 'Jueves') ? 'checked' : '';
									$checked_5 = ($dia == 'Viernes') ? 'checked' : '';
									$checked_6 = ($dia == 'Sabado') ? 'checked' : '';

									?>
									<tr class="text-center">
										<td><?=$id_clase?></td>
										<td><?=$nom_profesor?></td>
										<td><?=$nom_estudiante?></td>
										<td><?=$fecha_inicio?></td>
										<td><?=$fecha_fin?></td>
										<td><?=$dia?></td>
										<td><?=$horas?></td>
										<td><?=$observacion?></td>
										<td>
											<?=$span?>
										</td>
										<td>
											<div class="btn-group">
												<button class="btn btn-success btn-sm <?=$ver_aprobado?>" data-tooltip="tooltip" title="Aprobar clase" data-placement="bottom" data-trigger="hover" data-toggle="modal" data-target="#aprobar_clase<?=$id_clase?>">
													<i class="fa fa-check"></i>
												</button>
												<button class="btn btn-purple btn-sm <?=$ver_editar?>" data-tooltip="tooltip" title="Editar" data-placement="bottom" data-trigger="hover" data-toggle="modal" data-target="#editar_clase<?=$id_clase?>">
													<i class="fa fa-edit"></i>
												</button>
												<!-- <button class="btn btn-danger btn-sm" type="button" data-tooltip="tooltip" title="Cancelar Clase" data-placement="bottom">
													<i class="fa fa-times"></i>
												</button> -->
											</div>
										</td>
									</tr>



									<div class="modal fade" id="editar_clase<?=$id_clase?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
										<div class="modal-dialog modal-lg" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<h5 class="modal-title text-purple font-weight-bold" id="exampleModalLongTitle">Editar clase</h5>
												</div>
												<form method="POST">
													<input type="hidden" name="id_clase" value="<?=$id_clase?>">
													<input type="hidden" name="id_log" value="<?=$id_log?>">
													<div class="modal-body">
														<div class="row p-2">
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold">Profesor <span class="text-danger">*</span></label>
																<select name="profesor_edit" class="form-control" required>
																	<option value="<?=$id_profesor?>" selected class="d-none"><?=$nom_profesor?></option>
																	<?php
																	foreach ($datos_usuarios as $usuario) {
																		$id_user      = $usuario['id_user'];
																		$nom_completo = $usuario['nombre'] . ' ' . $usuario['apellido'];
																		$activo       = $usuario['activo'];

																		$ver = ($activo == 1 && $usuario['perfil'] == 3) ? '' : 'd-none';
																		?>
																		<option value="<?=$id_user?>" class="<?=$ver?>"><?=$nom_completo?></option>
																		<?php
																	}
																	?>
																</select>
															</div>
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold">Estudiante <span class="text-danger">*</span></label>
																<input type="text" value="<?=$nom_estudiante?>" class="form-control" disabled>
															</div>
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold">Horas al dia <span class="text-danger">*</span></label>
																<input type="text" value="<?=$horas?>" class="form-control" name="horas" required>
															</div>
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold">Fecha Inicio Semana <span class="text-danger">*</span></label>
																<input type="date" value="<?=$fecha_inicio?>" class="form-control fecha_inicio" name="fecha_inicio">
															</div>
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold">Fecha Fin Semana <span class="text-danger">*</span></label>
																<input type="date" name="fecha_fin" class="form-control fecha_fin" value="<?=$fecha_fin?>" readonly>
															</div>
															<div class="col-lg-12 form-group mt-2">
																<label class="font-weight-bold">Dia de la semana <span class="text-danger">*</span></label>
																<div class="form-inline">
																	<div class="custom-control custom-switch ml-3 mb-2">
																		<input type="radio" class="custom-control-input" name="dia" <?=$checked_1?> value="1" id="lunes_<?=$id_clase?>">
																		<label class="custom-control-label" for="lunes_<?=$id_clase?>">Lunes</label>
																	</div>
																	<div class="custom-control custom-switch ml-3 mb-2">
																		<input type="radio" class="custom-control-input" name="dia" <?=$checked_2?> value="2" id="martes_<?=$id_clase?>">
																		<label class="custom-control-label" for="martes_<?=$id_clase?>">Martes</label>
																	</div>
																	<div class="custom-control custom-switch ml-3 mb-2">
																		<input type="radio" class="custom-control-input" name="dia" <?=$checked_3?> value="3" id="miercoles_<?=$id_clase?>">
																		<label class="custom-control-label" for="miercoles_<?=$id_clase?>">Miercoles</label>
																	</div>
																	<div class="custom-control custom-switch ml-3 mb-2">
																		<input type="radio" class="custom-control-input" name="dia" <?=$checked_4?> value="4" id="jueves_<?=$id_clase?>">
																		<label class="custom-control-label" for="jueves_<?=$id_clase?>">Jueves</label>
																	</div>
																	<div class="custom-control custom-switch ml-3 mb-2">
																		<input type="radio" class="custom-control-input" name="dia" <?=$checked_5?> value="5" id="viernes_<?=$id_clase?>">
																		<label class="custom-control-label" for="viernes_<?=$id_clase?>">Viernes</label>
																	</div>
																	<div class="custom-control custom-switch ml-3 mb-2">
																		<input type="radio" class="custom-control-input" name="dia" <?=$checked_6?> value="6" id="sabado_<?=$id_clase?>">
																		<label class="custom-control-label" for="sabado_<?=$id_clase?>">Sabado</label>
																	</div>
																	<div class="custom-control custom-switch ml-3 mb-2">
																		<input type="radio" class="custom-control-input" name="dia" <?=$checked_0?> value="0" id="domingo_<?=$id_clase?>">
																		<label class="custom-control-label" for="domingo_<?=$id_clase?>">Domingo</label>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="modal-footer border-0">
														<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
															<i class="fa fa-times"></i>
															&nbsp;
															Cerrar
														</button>
														<button type="submit" class="btn btn-purple btn-sm">
															<i class="fa fa-save"></i>
															&nbsp;
															Guardar
														</button>
													</div>
												</form>
											</div>
										</div>
									</div>


									<div class="modal fade" id="aprobar_clase<?=$id_clase?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
										<div class="modal-dialog modal-lg" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<h5 class="modal-title text-purple font-weight-bold" id="exampleModalLongTitle">Aprobar clase</h5>
												</div>
												<form method="POST">
													<input type="hidden" name="id_clase" value="<?=$id_clase?>">
													<div class="modal-body">
														<div class="row p-2">
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold">Profesor <span class="text-danger">*</span></label>
																<input type="text" value="<?=$nom_profesor?>" class="form-control" disabled>
															</div>
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold">Estudiante <span class="text-danger">*</span></label>
																<input type="text" value="<?=$nom_estudiante?>" class="form-control" disabled>
															</div>
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold">Horas <span class="text-danger">*</span></label>
																<input type="text" value="<?=$horas?>" class="form-control" disabled>
															</div>
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold">Fecha incio (semana)<span class="text-danger">*</span></label>
																<input type="text" class="form-control" value="<?=$fecha_inicio?>" disabled>
															</div>
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold">Fecha fin (semana)<span class="text-danger">*</span></label>
																<input type="text" class="form-control" value="<?=$fecha_fin?>" disabled>
															</div>
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold">Dia (semana)<span class="text-danger">*</span></label>
																<input type="text" class="form-control" value="<?=$dia?>" disabled>
															</div>
															<div class="col-lg-12 form-group">
																		<label class="font-weight-bold">Evidencia</label>
																		<br>
																		<img src="<?=PUBLIC_PATH?>upload/<?=$clase['evidencia']?>" style="width: 100%;" alt="">
																	</div>
															<div class="col-lg-12 form-group">
																<label class="font-weight-bold">Observacion</label>
																<textarea name="observacion" class="form-control" rows="5"><?=$observacion?></textarea>
															</div>
														</div>
														<div class="modal-footer border-0">
															<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
																<i class="fa fa-times"></i>
																&nbsp;
																Cerrar
															</button>
															<button type="submit" class="btn btn-purple btn-sm">
																<i class="fa fa-check"></i>
																&nbsp;
																Aprobar
															</button>
														</div>
													</form>
												</div>
											</div>
										</div>
									</div>
									<?php
								}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
include_once VISTA_PATH . 'modulos' . DS . 'clases' . DS . 'agregarClase.php';

if (isset($_POST['profesor'])) {
	$instancia->registrarClaseControl();
}

if (isset($_POST['observacion'])) {
	$instancia->aprobarClaseControl();
}

if (isset($_POST['profesor_edit'])) {
	$instancia->editarClaseControl();
}
?>
<script src="<?=PUBLIC_PATH?>js/clases/funcionesClases.js"></script>