<!--Agregar usuario-->
<div class="modal fade" id="agregar_clase" tabindex="-1" role="dialog" aria-hidden="true" aria-labelledby="exampleModalLabel">
	<div class="modal-dialog modal-lg p-2" role="document">
		<div class="modal-content">
			<form method="POST">
				<input type="hidden" value="<?=$_SESSION['id']?>" name="id_log">
				<div class="modal-header p-3">
					<h4 class="modal-title text-purple font-weight-bold">Programar Clase</h4>
				</div>
				<div class="modal-body border-0">
					<div class="row  p-3">
						<div class="col-lg-6 form-group">
							<label class="font-weight-bold">Profesor <span class="text-danger">*</span></label>
							<select name="profesor" class="form-control" required>
								<option value="" selected>Seleccione una opcion...</option>
								<?php
								foreach ($datos_usuarios as $usuario) {
									$id_user      = $usuario['id_user'];
									$nom_completo = $usuario['nombre'] . ' ' . $usuario['apellido'];
									$activo       = $usuario['activo'];

									$ver = ($activo == 1 && $usuario['perfil'] == 3) ? '' : 'd-none';
									?>
									<option value="<?=$id_user?>" class="<?=$ver?>"><?=$nom_completo?></option>
									<?php
								}
								?>
							</select>
						</div>
						<div class="col-lg-6 form-group">
							<label class="font-weight-bold">Estudiante <span class="text-danger">*</span></label>
							<select name="estudiante" class="form-control" required>
								<option value="" selected>Seleccione una opcion...</option>
								<?php
								foreach ($datos_usuarios as $usuario) {
									$id_user      = $usuario['id_user'];
									$nom_completo = $usuario['nombre'] . ' ' . $usuario['apellido'];
									$activo       = $usuario['activo'];

									$ver = ($activo == 1 && $usuario['perfil'] == 4) ? '' : 'd-none';
									?>
									<option value="<?=$id_user?>" class="<?=$ver?>"><?=$nom_completo?></option>
									<?php
								}
								?>
							</select>
						</div>
						<div class="col-lg-6 form-group">
							<label class="font-weight-bold">Horas al dia <span class="text-danger">*</span></label>
							<input type="text" class="form-control" name="horas" required>
						</div>
						<div class="col-lg-6 form-group">
							<label class="font-weight-bold">Fecha inicio semana <span class="text-danger">*</span></label>
							<input type="date" name="fecha_inicio" class="form-control fecha_inicio" required>
						</div>
						<div class="col-lg-6 form-group">
							<label class="font-weight-bold">Fecha fin semana <span class="text-danger">*</span></label>
							<input type="date" name="fecha_fin" class="form-control fecha_fin" required readonly>
						</div>
						<div class="col-lg-12 form-group mt-2">
							<label class="font-weight-bold">Dias de la semana <span class="text-danger">*</span></label>
							<div class="form-inline">
								<div class="custom-control custom-switch ml-3 mb-2">
									<input type="checkbox" class="custom-control-input" name="dia[]" value="1" id="lunes">
									<label class="custom-control-label" for="lunes">Lunes</label>
								</div>
								<div class="custom-control custom-switch ml-3 mb-2">
									<input type="checkbox" class="custom-control-input" name="dia[]" value="2" id="martes">
									<label class="custom-control-label" for="martes">Martes</label>
								</div>
								<div class="custom-control custom-switch ml-3 mb-2">
									<input type="checkbox" class="custom-control-input" name="dia[]" value="3" id="miercoles">
									<label class="custom-control-label" for="miercoles">Miercoles</label>
								</div>
								<div class="custom-control custom-switch ml-3 mb-2">
									<input type="checkbox" class="custom-control-input" name="dia[]" value="4" id="jueves">
									<label class="custom-control-label" for="jueves">Jueves</label>
								</div>
								<div class="custom-control custom-switch ml-3 mb-2">
									<input type="checkbox" class="custom-control-input" name="dia[]" value="5" id="viernes">
									<label class="custom-control-label" for="viernes">Viernes</label>
								</div>
								<div class="custom-control custom-switch ml-3 mb-2">
									<input type="checkbox" class="custom-control-input" name="dia[]" value="6" id="sabado">
									<label class="custom-control-label" for="sabado">Sabado</label>
								</div>
								<div class="custom-control custom-switch ml-3 mb-2">
									<input type="checkbox" class="custom-control-input" name="dia[]" value="0" id="domingo">
									<label class="custom-control-label" for="domingo">Domingo</label>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer border-0">
					<button class="btn btn-danger btn-sm" data-dismiss="modal" type="button">
						<i class="fa fa-times"></i>
						&nbsp;
						Cancelar
					</button>
					<button type="submit" class="btn btn-purple btn-sm">
						<i class="fa fa-save"></i>
						&nbsp;
						Registrar
					</button>
				</div>
			</form>
		</div>
	</div>
</div>