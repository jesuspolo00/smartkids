<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';

$permisos = $instancia_permiso->permisosUsuarioControl(1, $perfil_log);
if ($permisos) {
	include_once VISTA_PATH . 'modulos' . DS . 'configuracion' . DS . 'index.php';
}

$permisos = $instancia_permiso->permisosUsuarioControl(6, $perfil_log);
if ($permisos) {
	include_once VISTA_PATH . 'modulos' . DS . 'inicio' . DS . 'estudiante.php';
}

$permisos = $instancia_permiso->permisosUsuarioControl(7, $perfil_log);
if ($permisos) {
	include_once VISTA_PATH . 'modulos' . DS . 'inicio' . DS . 'profesor.php';
}

$permisos = $instancia_permiso->permisosUsuarioControl(9, $perfil_log);
if ($permisos) {
	include_once VISTA_PATH . 'modulos' . DS . 'inicio' . DS . 'resenas.php';
}

$permisos = $instancia_permiso->permisosUsuarioControl(8, $perfil_log);
if ($permisos) {
	include_once VISTA_PATH . 'modulos' . DS . 'inicio' . DS . 'calendario.php';
}

$permisos = $instancia_permiso->permisosUsuarioControl(10, $perfil_log);
if ($permisos) {
	include_once VISTA_PATH . 'modulos' . DS . 'inicio' . DS . 'historial.php';
}
include_once VISTA_PATH . 'script_and_final.php';


?>
<script src="<?=PUBLIC_PATH?>js/calendario/funcionesCalendario.js"></script>