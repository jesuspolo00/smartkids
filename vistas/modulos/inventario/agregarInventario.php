<!-- Modal -->
<div class="modal fade" id="agregar_inventario" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title font-weight-bold text-purple" id="exampleModalLabel">Agregar inventario</h5>
      </div>
      <form method="POST">
        <input type="hidden" name="id_log" value="<?=$id_log?>">
        <div class="modal-body">
          <div class="row p-2">
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Descripcion <span class="text-danger">*</span></label>
              <input type="text" class="form-control" name="descripcion" maxlength="50" required>
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Cantidad <span class="text-danger">*</span></label>
              <input type="text" class="form-control numeros" name="cantidad" required>
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold">Profesor <span class="text-danger">*</span></label>
              <select name="usuario_id" class="form-control" data-tooltip="tooltip" title="Profesor" data-trigger="hover" required>
                <option value="" selected>Seleccione una opcion...</option>
                <?php
                foreach ($datos_usuarios as $usuario) {
                  $id_user = $usuario['id_user'];
                  $nombre  = $usuario['nombre'] . ' ' . $usuario['apellido'];
                  $activo  = $usuario['activo'];

                  $ver = ($activo == 1 && $usuario['perfil'] == 3) ? '' : 'd-none';
                  ?>
                  <option value="<?=$id_user?>" class="<?=$ver?>"><?=$nombre?></option>
                  <?php
                }
                ?>
              </select>
            </div>
          </div>
        </div>
        <div class="modal-footer border-0">
          <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
            <i class="fa fa-times"></i>
            &nbsp;
            Cerrar
          </button>
          <button type="submit" class="btn btn-success btn-sm">
            <i class="fa fa-save"></i>
            &nbsp;
            Guardar
          </button>
        </div>
      </form>
    </div>
  </div>
</div>
