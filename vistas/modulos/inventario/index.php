<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
include_once CONTROL_PATH . 'usuario' . DS . 'ControlUsuario.php';
include_once CONTROL_PATH . 'inventario' . DS . 'ControlInventario.php';

$instancia         = ControlInventario::singleton_inventario();
$instancia_usuario = ControlUsuario::singleton_usuario();

$datos_usuarios = $instancia_usuario->mostrarDatosUsuariosControl();

if (isset($_POST['usuario'])) {

	$usuario  = $_POST['usuario'];
	$articulo = $_POST['articulo'];

	$buscar = $instancia->mostrarDatosInventarioBuscarControl($usuario, $articulo);

} else {
	$buscar = $instancia->mostrarDatosInventarioControl();
}

$permisos = $instancia_permiso->permisosUsuarioControl(4, $perfil_log);

if (!$permisos) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	exit();
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow mb-4">
				<!-- Card Header - Dropdown -->
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-purple">
						<a href="<?=BASE_URL?>configuracion/index" class="text-decoration-none">
							<i class="fa fa-arrow-left text-purple"></i>
						</a>
						&nbsp;
						Inventario
					</h4>
					<div class="dropdown no-arrow">
						<a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
						</a>
						<div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
							<div class="dropdown-header">Acciones:</div>
							<a class="dropdown-item" href="#" data-toggle="modal" data-target="#agregar_inventario">Agregar inventario</a>
						</div>
					</div>
				</div>
				<div class="card-body">
					<form method="POST">
						<div class="row">
							<div class="col-lg-4 form-group">
							</div>
							<div class="col-lg-4 form-group">
								<select name="usuario" class="form-control" data-tooltip="tooltip" title="Profesor" data-trigger="hover">
									<option value="" selected>Seleccione una opcion...</option>
									<?php
									foreach ($datos_usuarios as $usuario) {
										$id_user = $usuario['id_user'];
										$nombre  = $usuario['nombre'] . ' ' . $usuario['apellido'];
										$activo  = $usuario['activo'];

										$ver = ($activo == 1 && $usuario['perfil'] == 3) ? '' : 'd-none';
										?>
										<option value="<?=$id_user?>" class="<?=$ver?>"><?=$nombre?></option>
										<?php
									}
									?>
								</select>
							</div>
							<div class="col-lg-3 form-group">
								<div class="input-group mb-3">
									<input type="text" class="form-control filtro" placeholder="Buscar" name="articulo">
								</div>
							</div>
							<div class="col-lg-1 form-group">
								<button class="btn btn-primary btn-sm mt-1">
									<i class="fa fa-search"></i>
									&nbsp;
									Buscar
								</button>
							</div>
						</div>
					</form>
					<div class="table-responsive">
						<table class="table table-hover border table-sm" width="100%" cellspacing="0">
							<thead>
								<tr class="text-center font-weight-bold">
									<th scope="col">#</th>
									<th scope="col">Profesor</th>
									<th scope="col">Articulo</th>
									<th scope="col">Cantidad</th>
									<th scope="col">Estado</th>
								</tr>
							</thead>
							<tbody class="buscar">
								<?php
								foreach ($buscar as $articulo) {
									$id_articulo = $articulo['id'];
									$descripcion = $articulo['descripcion'];
									$cantidad    = $articulo['cantidad'];
									$profesor    = $articulo['nom_usuario'];
									$estado      = $articulo['estado'];

									$ver_boton   = '';
									$ver_aprobar = 'd-none';

									if ($estado == 0) {
										$span        = '<span class="badge badge-secondary">Nuevo</span>';
										$ver_aprobar = 'd-none';
									}

									if ($estado == 1 || $cantidad == 0) {
										$span        = '<span class="badge badge-danger">Agotado</span>';
										$ver_boton   = 'd-none';
										$ver_aprobar = 'd-none';
									}

									if ($estado == 3) {
										$span        = '<span class="badge badge-success">Solicitado</span>';
										$ver_boton   = 'd-none';
										$ver_aprobar = '';
									}

									if ($estado == 4) {
										$span        = '<span class="badge badge-success">Aprobado</span>';
										$ver_boton   = '';
										$ver_aprobar = 'd-none';
									}

									?>
									<tr class="text-center articulo<?=$id_articulo?>">
										<td><?=$id_articulo?></td>
										<td><?=$profesor?></td>
										<td><?=$descripcion?></td>
										<td><?=$cantidad?></td>
										<td class="span<?=$id_articulo?>">
											<?=$span?>
										</td>
										<td>
											<div class="btn-group" role="group" aria-label="Basic example">
												<button class="btn btn-success btn-sm" data-tooltip="tooltip" title="Solicitar articulo" data-toggle="modal" data-target="#soli<?=$id_articulo?>">
													<i class="fas fa-plus"></i>
												</button>
												<button class="btn btn-danger btn-sm <?=$ver_boton?>" data-tooltip="tooltip" title="Disminuir cantidad" data-trigger="hover" data-placement="bottom" data-toggle="modal" data-target="#dismi<?=$id_articulo?>">
													<i class="fa fa-minus"></i>
												</button>
												<button class="btn btn-secondary btn-sm eliminar" data-tooltip="tooltip" title="Eliminar" data-placement="bottom" data-trigger="hover" id="<?=$id_articulo?>">
													<i class="fa fa-trash"></i>
												</button>
											</div>
										</td>
										<td>
											<button class="btn btn-success btn-sm aprobar <?=$ver_aprobar?>" data-tooltip="tooltip" title="Aprobar solicitud" data-placement="bottom" data-trigger="hover" id="<?=$id_articulo?>">
												<i class="fa fa-check"></i>
												&nbsp;
												Aprobar
											</button>
										</td>
									</tr>


									<!-- Modal -->
									<div class="modal fade" id="soli<?=$id_articulo?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
										<div class="modal-dialog modal-lg" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<h5 class="modal-title font-weight-bold text-purple" id="exampleModalLabel">Solicitar articulo</h5>
												</div>
												<form method="POST">
													<input type="hidden" name="id_log" value="<?=$id_log?>">
													<input type="hidden" name="id_articulo" value="<?=$id_articulo?>">
													<input type="hidden" name="estado" value="3">
													<input type="hidden" name="inicio" value="0">
													<div class="modal-body">
														<div class="row p-2">
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold">Articulo</label>
																<input type="text" class="form-control" value="<?=$descripcion?>" disabled>
															</div>
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold">Profesor</label>
																<input type="text" class="form-control" value="<?=$profesor?>" disabled>
															</div>
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold">Cantidad</label>
																<input type="text" class="form-control" value="<?=$cantidad?>" readonly name="cant_actual">
															</div>
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold">Cantidad a solicitar <span class="text-danger">*</span></label>
																<input type="text" name="cant_dism" required class="form-control numeros" value="0">
															</div>
														</div>
													</div>
													<div class="modal-footer border-0">
														<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
															<i class="fa fa-times"></i>
															&nbsp;
															Cerrar
														</button>
														<button type="submit" class="btn btn-success btn-sm">
															<i class="fa fa-save"></i>
															&nbsp;
															Guardar
														</button>
													</div>
												</form>
											</div>
										</div>
									</div>


									<!-- Modal -->
									<div class="modal fade" id="dismi<?=$id_articulo?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
										<div class="modal-dialog modal-lg" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<h5 class="modal-title font-weight-bold text-purple" id="exampleModalLabel">Disminuir articulo</h5>
												</div>
												<form method="POST">
													<input type="hidden" name="id_log" value="<?=$id_log?>">
													<input type="hidden" name="id_articulo" value="<?=$id_articulo?>">
													<input type="hidden" name="estado" value="0">
													<input type="hidden" name="inicio" value="0">
													<div class="modal-body">
														<div class="row p-2">
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold">Articulo</label>
																<input type="text" class="form-control" value="<?=$descripcion?>" disabled>
															</div>
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold">Profesor</label>
																<input type="text" class="form-control" value="<?=$profesor?>" disabled>
															</div>
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold">Cantidad</label>
																<input type="text" class="form-control" value="<?=$cantidad?>" readonly name="cant_actual">
															</div>
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold">Cantidad a disminuir (cantidad maxima <?=$cantidad?>) <span class="text-danger">*</span></label>
																<input type="text" name="cant_dism" required class="form-control numeros" value="0">
															</div>
														</div>
													</div>
													<div class="modal-footer border-0">
														<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
															<i class="fa fa-times"></i>
															&nbsp;
															Cerrar
														</button>
														<button type="submit" class="btn btn-success btn-sm">
															<i class="fa fa-save"></i>
															&nbsp;
															Guardar
														</button>
													</div>
												</form>
											</div>
										</div>
									</div>

									<?php
								}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
include_once VISTA_PATH . 'modulos' . DS . 'inventario' . DS . 'agregarInventario.php';

if (isset($_POST['descripcion'])) {
	$instancia->registrarInventarioControl();
}

if (isset($_POST['id_articulo'])) {
	$instancia->estadoArticuloControl();
}
?>
<script src="<?=PUBLIC_PATH?>js/inventario/funcionesInventario.js"></script>