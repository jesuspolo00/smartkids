<?php
date_default_timezone_set('America/Bogota');

inicio_fin_semana($_POST['fecha']);

function inicio_fin_semana($fecha)
{

	$diaInicio = "Monday";
	$diaFin    = "Sunday";

	$strFecha = strtotime($fecha);

	$fechaInicio = date('Y-m-d', strtotime('last ' . $diaInicio, $strFecha));
	$fechaFin    = date('Y-m-d', strtotime('next ' . $diaFin, $strFecha));

	if (date("l", $strFecha) == $diaInicio) {
		$fechaInicio = date("Y-m-d", $strFecha);
	}
	if (date("l", $strFecha) == $diaFin) {
		$fechaFin = date("Y-m-d", $strFecha);
	}

	echo $fechaFin;

}
