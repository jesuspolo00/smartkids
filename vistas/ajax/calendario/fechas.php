<?php
header('Content-Type: application/json');
define('DS', DIRECTORY_SEPARATOR);
define('ROOT', '..' . DS . '..' . DS . '..');
require_once '..' . DS . '..' . DS . '..' . DS . 'confi' . DS . 'Config.php';
require_once CONTROL_PATH . 'clases' . DS . 'ControlClases.php';
require_once CONTROL_PATH . 'numeros.php';

$instancia    = ControlClases::singleton_clases();
$datos_clases = $instancia->mostrarClasesSemanasControl();

$fechas = [];

foreach ($datos_clases as $clase) {

	$fecha_inicio_semana = inicio_semana($clase['fecha_inicio']);
	$fecha_fin_semana    = fin_semana($clase['fecha_fin']);

	$fecha_inicio_semana_sistema = inicio_semana(date('Y-m-d'));
	$fecha_fin_semana_sistema    = fin_semana(date('Y-m-d'));

	if ($fecha_inicio_semana_sistema == $fecha_inicio_semana) {

		$dia_clase = $clase['dia'];

		$fecha_mes_clase = date('Y-', strtotime($fecha_fin_semana));
		$fecha_dia_hoy   = $fecha_mes_clase . date('m-d');

		$dia_hoy = date('w', strtotime($fecha_dia_hoy));

		if ($dia_hoy == $clase['dia']) {

			$fechas[] = array(
				'title' => 'Clase - ' . $clase['nom_profesor'] . ' - ' . $clase['nom_estudiante'],
				'start' => $fecha_dia_hoy,
				'end'   => $fecha_dia_hoy,
				'color' => '#5c5f61',
			);

		}

		if ($dia_hoy < $clase['dia']) {

			$resta_dias = $clase['dia'] - $dia_hoy;

			$fecha_futura = date("Y-m-d", strtotime($fecha_dia_hoy . "+ " . $resta_dias . " days"));

			$fechas[] = array(
				'title' => 'Clase - ' . $clase['nom_profesor'] . ' - ' . $clase['nom_estudiante'],
				'start' => $fecha_futura,
				'end'   => $fecha_futura,
				'color' => '#f39c12',
			);

		}

		if ($dia_hoy > $clase['dia']) {

			$color = ($clase['estado'] == 1 || $clase['estado'] == 3) ? '#27AE60' : '#cb4335';
			$resta_dias = $dia_hoy - $clase['dia'];

			$fecha_futura = date("Y-m-d", strtotime($fecha_dia_hoy . "- " . $resta_dias . " days"));

			$fechas[] = array(
				'title' => 'Clase - ' . $clase['nom_profesor'] . ' - ' . $clase['nom_estudiante'],
				'start' => $fecha_futura,
				'end'   => $fecha_futura,
				'color' => $color,
			);

		}
	}
}

echo json_encode($fechas);
