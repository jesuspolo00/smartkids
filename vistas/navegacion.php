<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
  $er    = '2';
  $error = base64_encode($er);
  $salir = new Session;
  $salir->iniciar();
  $salir->outsession();
  header('Location:login?er=' . $error);
  exit();
}
include_once VISTA_PATH . 'cabeza.php';
require_once CONTROL_PATH . 'permisos' . DS . 'ControlPermisos.php';

$instancia_permiso = ControlPermisos::singleton_permisos();

$id_log     = $_SESSION['id'];
$perfil_log = $_SESSION['rol'];

$datos_foto  = $instancia_permiso->fotoPerfilControl($id_log);
$foto_perfil = ($datos_foto['nombre'] == '') ? 'img/user.svg' : 'upload/' . $datos_foto['nombre'];

?>
<!-- Sidebar -->
<ul class="navbar-nav bg-white sidebar sidebar-dark accordion" id="accordionSidebar">

  <!-- Sidebar - Brand -->
  <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?=BASE_URL?>inicio">
    <div class="sidebar-brand-icon">
      <img src="<?=PUBLIC_PATH?>img/logo.png" alt="" width="60">
    </div>
    <div class="sidebar-brand-text mx-3 text-primary mt-3">
    </div>
  </a>

  <!-- Divider -->
  <hr class="sidebar-divider my-0">

  <!-- Nav Item - Dashboard -->
  <li class="nav-item active">
    <a class="nav-link" href="<?=BASE_URL?>inicio">
      <i class="fas fa-home text-purple"></i>
      <span class="text-muted">Inicio</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider bg-gray">

    <?php
    $permisos = $instancia_permiso->permisosUsuarioControl(3, $perfil_log);
    if ($permisos) {
      ?>
      <li class="nav-item">
        <a class="nav-link collapsed" href="<?=BASE_URL?>clases/index" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="fas fa-calendar-alt text-purple"></i>
          <span class="text-muted">Clases</span>
        </a>
      </li>
    <?php }
    $permisos = $instancia_permiso->permisosUsuarioControl(4, $perfil_log);
    if ($permisos) {
      ?>
      <li class="nav-item">
        <a class="nav-link collapsed" href="<?=BASE_URL?>inventario/index" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="fas fa-cubes text-purple"></i>
          <span class="text-muted">Inventario</span>
        </a>
      </li>
    <?php }?>
    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block bg-gray">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
      <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

  </ul>
  <!-- End of Sidebar -->

  <!-- Content Wrapper -->
  <div id="content-wrapper" class="d-flex flex-column">

    <!-- Main Content -->
    <div id="content">

      <!-- Topbar -->
      <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow-none">


        <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
          <i class="fa fa-bars text-primary"></i>
        </button>

        <!-- Topbar Navbar -->
        <ul class="navbar-nav ml-auto">

          <!-- Nav Item - Search Dropdown (Visible Only XS) -->
          <li class="nav-item dropdown no-arrow d-sm-none">
            <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="fas fa-search fa-fw"></i>
            </a>
            <!-- Dropdown - Messages -->
            <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
              <form class="form-inline mr-auto w-100 navbar-search">
                <div class="input-group">
                  <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                  <div class="input-group-append">
                    <button class="btn btn-success" type="button">
                      <i class="fas fa-search fa-sm"></i>
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </li>

          <div class="topbar-divider d-none d-sm-block"></div>

          <!-- Nav Item - User Information -->
          <li class="nav-item dropdown no-arrow">
            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <span class="mr-2 d-none d-lg-inline text-gray-600"><?=$_SESSION['nombre_admin'] . ' ' . $_SESSION['apellido']?></span>
              <div class="circular--perfil">
                <img src="<?=PUBLIC_PATH . $foto_perfil?>">
              </div>
            </a>
            <!-- Dropdown - User Information -->
            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
              <a class="dropdown-item" href="<?=BASE_URL?>perfil/index">
                <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                Perfil
              </a>
              <?php
              $permisos = $instancia_permiso->permisosUsuarioControl(1, $perfil_log);
              if ($permisos) {
                ?>
                <a class="dropdown-item" href="<?=BASE_URL?>configuracion/index">
                  <i class="fas fa-cog fa-sm fa-fw mr-2 text-gray-400"></i>
                  Configuracion
                </a>
              <?php }?>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="<?=BASE_URL?>salir">
                <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                Cerrar sesion
              </a>
            </div>
          </li>

        </ul>

      </nav>
      <!-- End of Topbar -->

      <!-- Begin Page Content -->
      <div class="container-fluid">

        <?php
        include_once VISTA_PATH . 'script_and_final.php';
      ?>